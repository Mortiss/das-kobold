﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GeometryMesh;
using System;

/// <summary>
/// A class to hold all the terrain meshes and other interesting things and do something with them.
/// </summary>
public class MyTerrain {
	public static MyTerrain theOne;
	public int lastBodyIndex = 0;
    public int lastChunkIndex = 0;

    public List<Body> Bodies { get; private set; }

    public MyTerrain () { 
        Bodies = new List<Body>();
		theOne = this;
    }

    public void AddBody (GeometryMesh.Mesh mesh)
    {
       
		Bodies.Add(new Body(lastBodyIndex, mesh));
		lastBodyIndex++;
    }

	/// <summary>
	/// Splits the body into several separate bodies if needed.
	/// </summary>
	/// <param name="body">Body to split.</param>
	public void DivideBody (Body body) {
		//floodfill chunks to another list
		//while floodfilling we shall detect stable chunks and mark the body accordingly
		bool unstable = true;
		List<TerrainChunk> newBody = new List<TerrainChunk> ();
		Queue<TerrainChunk> frontChunks = new Queue<TerrainChunk> ();

		try {
			frontChunks.Enqueue (body.Chunks [0]);
		}
		catch {
			Debug.LogError ("Trying to divide an empty body " + body.id);
			return;
		}

		while (frontChunks.Count>0)
		{
			TerrainChunk chunk = frontChunks.Dequeue ();
			newBody.Add (chunk);
			if (unstable && chunk.stable)
				unstable = false;
			body.Chunks.Remove (chunk);
			for (int i = 0; i < 3; i++) {
				TerrainChunk frontChunk = chunk.neighbours [i];
				if (frontChunk == null || frontChunks.Contains (frontChunk) || newBody.Contains (frontChunk))
					continue;
				else
					frontChunks.Enqueue (chunk.neighbours [i]);
			}
		}
		//if all the chunks were found we don't need to split the body any longer
		if (body.Chunks.Count == 0) {
			body.Chunks = newBody;
			return;
		}

		//create new body from this floodfill and move the chunks to another parent
		GameObject newParent = new GameObject ("body" + lastBodyIndex);
		Bodies.Add(new Body (lastBodyIndex, newBody, unstable, newParent));
		foreach (TerrainChunk chunk in newBody) {
			chunk.gameObject.transform.parent = newParent.transform;
		}
		lastBodyIndex++;

		//detect whether the remaining part is stable
		if (body.Chunks.Find(chunk => chunk.stable) == null)
			body.MakeUnstable ();

		//continue with division
		DivideBody(body);
	}

	/// <summary>
	/// Checks all the chunks for breaking, destroys stressed chunks, splits the bodies accordingly, resets dynamic stress, unite non-moving bodies and recalculates static stress.
	/// </summary>
	public void UpdateBodies () {
		//without border

		HashSet<Body> bodiesNotChanged = new HashSet<Body>();
		List<Body> bodiesToUpdate = new List<Body> (Bodies.Count);
		bodiesNotChanged.Add (Bodies[0]);

		for (int i = 1; i < Bodies.Count; i++) {
			Body body = Bodies [i];
			List<TerrainChunk> chunksToDestroy = new List<TerrainChunk> (body.Chunks.Count);
			foreach (TerrainChunk chunk in body.Chunks) {
				if (chunk.CheckForBreaking ())
					chunksToDestroy.Add (chunk);
			}
			if (chunksToDestroy != null) {
				body.DeleteChunk (chunksToDestroy);
				if (body.Chunks.Count != 0)
					bodiesToUpdate.Add (body);
			} else
				bodiesNotChanged.Add (body);
		}

		//reset dynamic stress
		ResetDynamicStress();

		foreach (Body body in bodiesToUpdate) {
			DivideBody (body);
		}

        //Unite Bodies
        Body bodyToUnite = null;
        while (true)
        {
            foreach (Body body in Bodies)
            {
                if (body.CheckIfUnstableAndSleeping())
                {
                    bodyToUnite = body;
                    break;
                }
            }
            if (bodyToUnite == null) 
                break;
            else
            {
                if (bodiesNotChanged.Contains(bodyToUnite))
                    bodiesNotChanged.Remove(bodyToUnite);
                bodyToUnite.MakeStable();
                bodyToUnite = null;
            }
        }

        //Bodies have changed!
        foreach (Body body in Bodies) {
			if (bodiesNotChanged.Contains (body))
				continue;
			body.CalculateStress ();
			body.VisualizeStress ();
		}
	}

	void ResetDynamicStress() {
		foreach (Body body in Bodies)
			foreach (TerrainChunk chunk in body.Chunks)
				chunk.DynamicStress = 0;
	}

	Body GetBodyByID (int id) {
		return Bodies.Find (body => body.id == id);
	}
}

/// <summary>
/// A container body for chunks in one mesh
/// </summary>
public class Body
{
	bool unstable = false;
    public List<TerrainChunk> Chunks {get; set;}
	public int id { get; private set;}
    float stabilityPercent = 0.7f;
	GameObject gameBody;
    Rigidbody2D rigidbody;

	float maxstress = 0; 

    public Body(int id, GeometryMesh.Mesh mesh)
    {
        this.id = id;
        Chunks = new List<TerrainChunk>();
		gameBody = new GameObject("body" + id);
        TerrainChunk newChunk;
        foreach (Triangle triangle in mesh.Triangles)
        {
            newChunk = AddChunk(triangle);
            if (id == 0)
                newChunk.stable = true;
        }
        CalculateNeighbouringChunks();
        if (id != 0) {
            CreateStableRegion();
            CalculateStress();
        }

        //   VisualizeStress();

    }

    TerrainChunk AddChunk (Triangle triangle)
    {
        //create prefab
        GameObject newChunk = GameObject.Instantiate(Resources.Load("chunk") as GameObject);
        TerrainChunk chunkScript = newChunk.GetComponent<TerrainChunk>();
        chunkScript.Init(MyTerrain.theOne.lastChunkIndex, triangle, this, gameBody);
        MyTerrain.theOne.lastChunkIndex++;
        Chunks.Add(chunkScript);
        return chunkScript;
    }

    TerrainChunk AddChunk (Vector2[] corners)
    {
        //create prefab
        GameObject newChunk = GameObject.Instantiate(Resources.Load("chunk") as GameObject);
        TerrainChunk chunkScript = newChunk.GetComponent<TerrainChunk>();
        chunkScript.Init(MyTerrain.theOne.lastChunkIndex, corners, this, gameBody);
        MyTerrain.theOne.lastChunkIndex++;
        Chunks.Add(chunkScript);
        return chunkScript;
    }

    public Body (int id, List<TerrainChunk> listOfChunks, bool unstable, GameObject parent) {
		gameBody = parent;
		this.id = id;
		Chunks = listOfChunks;
		this.unstable = unstable;
		if (unstable)
			MakeUnstable ();
		else {
			CalculateStress ();
		}
        //Change the chunks parent
        foreach (TerrainChunk chunk in Chunks)
            chunk.parent = this;
//		VisualizeStress ();

    }

    public TerrainChunk GetChunkByID (int chunkid)
    {
        return Chunks.Find(chunk => chunk.ID == chunkid);
    }

    /// <summary>
    /// Applies dynamic stress to chunks starting at the given chunk with the given amount of stress. 
    /// </summary>
    /// <param name="chunkid">ID of the initial chunk.</param>
    /// <param name="amount">Initial amount of stress.</param>
    public void ApplyDynamicStress(int chunkid, float amount)
    {
        TerrainChunk initialChunk = GetChunkByID(chunkid);
        if (initialChunk == null)
        {
            Debug.LogError("trying to apply stress to a non-existing chunk" + chunkid);
            return;
        }
        ApplyDynamicStress(initialChunk, amount);
    }

    /// <summary>
    /// Applies dynamic stress to chunks starting at the given chunk with the given amount of stress.
    /// </summary>
    /// <param name="initialChunk">Initial chunk.</param>
    /// <param name="amount">Amount.</param>
    public void ApplyDynamicStress (TerrainChunk initialChunk, float amount) { 
        if (!Chunks.Contains(initialChunk))
        {
            Debug.LogError("trying to apply stress to a chunk from the wrong body" + initialChunk.ID);
            return;
        }
        HashSet<TerrainChunk> currentChunks = new HashSet<TerrainChunk>();
		HashSet<TerrainChunk> nextChunks = new HashSet<TerrainChunk>();
		currentChunks.Add (initialChunk);
		//функция затухания
		float modifier = 0.5f;
		float threshold = 0.5f;
		int chunksAffected = 0;
		int frontsNum = 0;
		while (amount > threshold) {
			foreach (TerrainChunk chunk in currentChunks) {
				for (int i = 0; i<3; i++) {
					TerrainChunk newNeighbour = chunk.neighbours [i];
					if (newNeighbour!=null && !currentChunks.Contains(newNeighbour) && !nextChunks.Contains(newNeighbour))
						nextChunks.Add(newNeighbour);
					}
				chunk.DynamicStress += amount;
				chunksAffected++;
			}
			amount = amount * modifier;
			HashSet<TerrainChunk> helperPointer = currentChunks;
			currentChunks = nextChunks;
			nextChunks = helperPointer;
			nextChunks.Clear ();
			frontsNum++;
		}
		Debug.Log ("Applied dynamic stress to "+chunksAffected+" chunks in "+frontsNum+" fronts.");
	}

	public void MakeUnstable () {
		if (!unstable)
			unstable = true;

        //		MeshCollider collider = gameBody.AddComponent <MeshCollider> ();
        //		GeometryMesh.Mesh bodymesh = new GeometryMesh.Mesh (Chunks);
        //		collider.sharedMesh = bodymesh.ToUnityMesh ();
        //		collider.convex = true;

        if (rigidbody == null)
        {
            rigidbody = gameBody.AddComponent<Rigidbody2D>();
            rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            rigidbody.sleepMode = RigidbodySleepMode2D.StartAwake;
        }
        else
            rigidbody.isKinematic = false;

//		foreach (TerrainChunk chunk in Chunks) {
//			if (chunk.Type != TerrainChunk.ChunkType.Inner)
//				chunk.GameChunk.
//		}
	}

    /// <summary>
    /// Makes the body stable - unites it with the surrounding bodies. Intended to be used with rigidbody.isSleeping check.
    /// </summary>
    public void MakeStable ()
    {
        //if it was stable - do nothing
        if (!unstable) return;
        unstable = false;

        if (UniteWithNeighbours())
            Debug.Log("body"+id+" has been united!");

        rigidbody.isKinematic = true;
    }

    public bool CheckIfUnstableAndSleeping ()
    {
        if (!unstable) 
            return false;
        if (rigidbody == null || rigidbody.IsAwake())
            return false;
        return true;
    }

    public void Delete () {
        Chunks.Clear();
		GameObject.Destroy (gameBody);
		MyTerrain.theOne.Bodies.Remove (this);
	}

	public void DeleteChunk (List<TerrainChunk> chunks) {
		foreach (TerrainChunk chunk in chunks) {
			DeleteChunk (chunk);
		}
	}

	public void DeleteChunk (TerrainChunk chunk) {
		//delete the chunk from the Chunks list and all chunk neighbours.
		chunk.Delete ();
		chunk.triangle.Delete ();
		Chunks.Remove (chunk);

		//if it was the last chunk - delete the body alltogether
		if (Chunks.Count == 0) {
			Delete ();
		}

		//if it was stable -> determine if there are other stable chunks
		if (chunk.stable && Chunks.Find (unit => unit.stable) == null)
			MakeUnstable ();

//		//detect path from neighbouring chunks to each other to determine if the body is still continuous
//		//if not - divide the body in two or three bodies
//		short numParts = 1;
//		if (chunk.neighbours[0] != null && 
//			chunk.neighbours[1] != null && 
//			ShortestPath (0, chunk.neighbours [0], chunk.neighbours [1]) == null)
//			numParts++;
//		if (chunk.neighbours[1] != null && 
//			chunk.neighbours[2] != null &&
//			ShortestPath (0, chunk.neighbours [1], chunk.neighbours [2]) == null)
//			numParts++;
//		if (chunk.neighbours[0] != null && 
//			chunk.neighbours[2] != null &&
//			ShortestPath (0, chunk.neighbours [0], chunk.neighbours [2]) == null)
//			numParts++;
//		if (numParts == 4)
//			numParts = 3;
//		else if (numParts == 3)
//			numParts = 2;
//		if (numParts>1)
//			MyTerrain.theOne.DivideBody(this);
//
//
//		//recalculate stress - we can optimize this by storing the tension paths for each chunk 
//		//and recalculate only those paths that had the deleted chunk in them. 
//		//Furthermore, we can do it every N frames and only if the bodies were modified 
//		//to be ready for the massive destruction of chunks.
//		if (!unstable) {
//			CalculateStress ();
//			VisualizeStress ();
//		}
	}

    public void DeleteChunk (int chunkID)
    {
		TerrainChunk chunk = GetChunkByID (chunkID);
		DeleteChunk (chunk);
    }

    private void CalculateNeighbouringChunks ()
    {
        foreach (TerrainChunk chunk in Chunks)
        {
            Triangle[] triangleNeighbours = chunk.triangle.GetNeighbours();
            for (int i =0; i<3; i++)
            {
                chunk.neighbours[i] = GetChunkByTriangle(triangleNeighbours[i]);
            }
        }
    }

	//use this to calculate the direction sorting function
    Vector2 firstStable;
    //the next 4 functions create a random stable region inside the body. We need it for testing, and it could be useful later.
    private void CreateStableRegion ()
    {
        List<TerrainChunk> testedChunks = new List<TerrainChunk>();
        TerrainChunk randomChunk = Chunks[UnityEngine.Random.Range(0, Chunks.Count-1)];
        firstStable = randomChunk.triangle.center;
        RecursiveStability(randomChunk, ref testedChunks);
        CloseUnstableGaps();
    }

    private void RecursiveStability (TerrainChunk chunk, ref List<TerrainChunk> testedChunks)
    {

        chunk.stable = true;

        foreach (TerrainChunk neighbour in chunk.neighbours)
        {
            if (neighbour != null && !neighbour.stable && !testedChunks.Contains(neighbour))
            {
				if (UnityEngine.Random.value < stabilityPercent)
                    RecursiveStability(neighbour, ref testedChunks);
                else
                    testedChunks.Add(neighbour);
            }
        }
    }

    private void CloseUnstableGaps()
    {
        foreach (TerrainChunk chunk in Chunks)
        {
            if (chunk.stable) continue;
            if (GetStableNeighboursCount(chunk) > 1)
            {
                chunk.stable = true;
            }
        }
    }

    private int GetStableNeighboursCount (TerrainChunk chunk)
    {
        int numStableNeighbours = 0;
        for (int i = 0; i < 3; i++)
            if (chunk.neighbours[i]!=null && chunk.neighbours[i].stable)
                numStableNeighbours++;
        return numStableNeighbours;
    }

    /// <summary>
    /// This function calculates shortest paths to the nearest stable chunk for all chunks and adds stress to chunks along those paths.
    /// </summary>
    public void CalculateStress ()
    {
		//for now - only calculate for stable bodies
		if (unstable)
			return;
		//clear stress
		foreach (TerrainChunk chunk in Chunks) {
			chunk.StaticStress = 0;
		}
        
       // int testedChunks = 0;
        foreach (TerrainChunk chunk in Chunks)
        {
            if (chunk.stable) continue;
            //try only border chunks
            if (chunk.Type == TerrainChunk.ChunkType.Inner) continue;
            TerrainChunk[] path = PathToNearestStable(chunk);
            if (path == null) continue;

            foreach (TerrainChunk pathChunk in path)
            {
                if (pathChunk != chunk && !pathChunk.stable)
                {
                    pathChunk.StaticStress += 1f;
					if (pathChunk.StaticStress > maxstress)
						maxstress = pathChunk.StaticStress;
                }
            }
            //TEST!!
         //  testedChunks++;
         //  if (testedChunks>)
          //     break;
        }
        //try without normalization
//        foreach (TerrainChunk chunk in Chunks)
//        {
//            chunk.Stress = chunk.Stress / maxStress;
//        }
	//	Debug.Log ("max stress calculated for body"+id+" is "+maxstress);
    }

    public void VisualizeStress()
    {
        foreach (TerrainChunk chunk in Chunks)
        {
			if (chunk.stable)
				chunk.ColorChunk (Color.black);
			else if (chunk.cracked)
				chunk.ColorChunk (Color.red);
			else
				chunk.ColorChunk (Color.white);
        }
    }



    public bool UniteWithNeighbours()
    {

        HashSet<int> bodiesToUnite = new HashSet<int>();
        HashSet<Vector2> checkedPoints = new HashSet<Vector2>();
        List<ContactPoint2D> cornerContacts = new List<ContactPoint2D>();

        bool doneSomething = false;
        foreach (TerrainChunk chunk in Chunks)
        {
            ContactPoint2D[] contactPoints;
            ushort num = chunk.GetContactsWithOtherBodies(out contactPoints);
            if (num == 0)
                continue;
            TerrainChunk otherChunk;
            for (int i = 0; i < num - 1; i++)
            {
                otherChunk = contactPoints[i].collider.gameObject.GetComponent<TerrainChunk>();
                bodiesToUnite.Add(otherChunk.parent.id);
                if (checkedPoints.Contains(contactPoints[i].point))
                    continue;
                //First we try to find two contact points between same chunks
                for (int j = i + 1; j < num; j++)
                {
                    if (contactPoints[i].collider == contactPoints[j].collider)
                    {
                        Debug.Log("Found two points between chunks " + contactPoints[i].collider.name + ", " + contactPoints[i].otherCollider.name);
                        checkedPoints.Add(contactPoints[i].point);
                        checkedPoints.Add(contactPoints[j].point);
                        doneSomething = SplitChunk(otherChunk, contactPoints[i].point, contactPoints[j].point, chunk);
                        doneSomething = doneSomething | SplitChunk(chunk, contactPoints[i].point, contactPoints[j].point, otherChunk);
                        break;
                    }
                }
            }

            otherChunk = contactPoints[num - 1].collider.gameObject.GetComponent<TerrainChunk>();
            bodiesToUnite.Add(otherChunk.parent.id);

            //Then we are left with corner contacts
            for (int i = 0; i < num; i++)
            {
                bool pointIsChecked = false;
                foreach (Vector2 point in checkedPoints)
                    if ((contactPoints[i].point - point).sqrMagnitude < 0.2f)
                        pointIsChecked = true;
                if (!pointIsChecked)
                    cornerContacts.Add(contactPoints[i]);
            }
        }


        Debug.Log("working on corner contacts below:");
        foreach (ContactPoint2D contact in cornerContacts)
        {
            TerrainChunk chunk1 = contact.collider.gameObject.GetComponent<TerrainChunk>();
            TerrainChunk chunk2 = contact.otherCollider.gameObject.GetComponent<TerrainChunk>();
            if (ConnectChunks(chunk1, chunk2, contact.point))
                doneSomething = true;
            Debug.Log(contact.collider.name + " " + contact.otherCollider.name + " " + contact.point);
        }

        if (doneSomething)
        //actually add the chunks from the bodiesToUnite
        foreach (int BodyID in bodiesToUnite)
        {
            Body body = MyTerrain.theOne.Bodies.Find(obj => obj.id == BodyID);
            AddChunks(body.Chunks);
            body.Delete();
        }
        return doneSomething;
    }

    void AddChunks (List<TerrainChunk> chunks)
    {
        Chunks.AddRange(chunks);
        foreach (TerrainChunk chunk in chunks)
        {
            chunk.parent = this;
            chunk.gameObject.transform.parent = gameBody.transform;
        }

    }

    /// <summary>
    /// Connects two chunks from different bodies that have only one contact point.
    /// </summary>
    /// <returns><c>true</c>, if chunks were connected, <c>false</c> otherwise.</returns>
    /// <param name="chunk1">Chunk1.</param>
    /// <param name="chunk2">Chunk2 (from foreign body).</param>
    /// <param name="point">Contact point.</param>
    bool ConnectChunks(TerrainChunk chunk1, TerrainChunk chunk2, Vector2 point)
    {
        //0 - undefined/error, 1 - chunk1 has a corner corresponding to the contact point, 2 - chunk2 - the same, 3 - both chunks 
        ushort caseNum = 0;
        //this is the index of a chunk corner that we are interested in (e.g. that corresponds to the contact point) for chunks 1 and 2
        ushort a1 = 10;
        ushort a2 = 10;
        Vector2[] corners1 = chunk1.GetCorners();
        Vector2[] corners2 = chunk2.GetCorners();
        for (ushort i = 0; i < 3; i++)
        {
            if (PointsAreClose(corners1[i], point)) {
                if (caseNum == 0)
                    caseNum = 1;
                else if (caseNum == 2)
                    caseNum = 3;
                a1 = i;
            }
            if (PointsAreClose(corners2[i], point))
            {
                if (caseNum == 0)
                    caseNum = 2;
                else if (caseNum == 1)
                    caseNum = 3;
                a2 = i;
            }
        }
        Debug.Log("chunks" + chunk1.ID + " and " + chunk2.ID + "has a contact case " + caseNum);
        TerrainChunk newChunk = null;
;       switch (caseNum)
        {
            case 0:
                return false;

            case 1:
                //the contact point must lie on one side of the second chunk, find the side!
                for (ushort i = 0; i < 3; i++) {
                    if (Vector3.Cross(point - corners2[i], corners2[(i + 1) % 3] - corners2[i]).z < Mathf.Epsilon)
                        a2 = i;
                }
                if (a2 == 10)
                {
                    Debug.LogError("Logic is wrong! Case1 corner routine");
                    return false;
                }
                //create the chunk
                if (chunk1.neighbours[a1] == null) //left 
                {
                    newChunk = AddChunk(new Vector2[3] { corners1[(a1 + 1) % 3], point, corners2[a2] });
                    newChunk.neighbours[0] = chunk1;
                    newChunk.neighbours[1] = chunk2;
                    chunk1.neighbours[a1] = newChunk;
                    SplitChunk(chunk2, point, corners2[a2], newChunk);
                }
                else //right
                { 
                    if (chunk1.neighbours[(a1 + 2) % 3] != null)
                    {   
                        Debug.LogError("Wrong again!"); 
                        return false;
                    }
                    newChunk = AddChunk(new Vector2[3] { corners1[(a1 + 2) % 3], corners2[(a2 + 1) % 3], point });
                    newChunk.neighbours[1] = chunk2;
                    newChunk.neighbours[2] = chunk1;
                    chunk1.neighbours[(a1 + 2) % 3] = newChunk;
                    SplitChunk(chunk2, point, corners2[(a2+1)%3], newChunk);
                }
                break;
            case 2:
                //the contact point must lie on one side of the first chunk, find the side!
                for (ushort i = 0; i < 3; i++)
                {
                    if (Vector3.Cross(point - corners1[i], corners1[(i + 1) % 3] - corners1[i]).z < Mathf.Epsilon)
                        a1 = i;
                }
                if (a1 == 10)
                {
                    Debug.LogError("Logic is wrong! Case2 corner routine");
                    return false;

                }
                //create the chunk
                if (chunk2.neighbours[a2] == null) //right 
                {
                    newChunk = AddChunk(new Vector2[3] { corners2[a2], corners1[a1], corners2[(a2+1)%3] });
                    newChunk.neighbours[0] = chunk1;
                    newChunk.neighbours[2] = chunk2;
                    chunk2.neighbours[a2] = newChunk;
                    SplitChunk(chunk1, point, corners1[a1], newChunk);
                }
                else //left
                {
                    if (chunk2.neighbours[(a2 + 2) % 3] != null)
                    {
                        Debug.LogError("Wrong again!");
                        return false;
                    }
                    newChunk = AddChunk(new Vector2[3] { corners2[a2], corners2[(a2 + 2) % 3], corners1[(a1+1)%3] });
                    newChunk.neighbours[0] = chunk2;
                    newChunk.neighbours[2] = chunk1;
                    chunk2.neighbours[(a2 + 2) % 3] = newChunk;
                    SplitChunk(chunk1, point, corners1[(a1 + 1) % 3], newChunk);
                }
                break;
            case 3:
                if (chunk1.neighbours[a1] == null) //left 
                {
                    newChunk = AddChunk(new Vector2[3] { corners1[(a1 + 1) % 3], point, corners2[(a2 + 2) % 3] });
                    newChunk.neighbours[0] = chunk1;
                    newChunk.neighbours[1] = chunk2;
                    chunk1.neighbours[a1] = newChunk;
                    chunk2.neighbours[(a2 + 2) % 3] = newChunk;
                }

                else //right
                {
                    if (chunk1.neighbours[(a1 + 2) % 3] != null)
                    {
                        Debug.LogError("Wrong again!");
                        return false;
                    }
                    newChunk = AddChunk(new Vector2[3] { corners1[(a1 + 2) % 3], corners2[(a2 + 1) % 3], point });
                    newChunk.neighbours[1] = chunk2;
                    newChunk.neighbours[2] = chunk1;
                    chunk1.neighbours[(a1+2)%3] = newChunk;
                    chunk2.neighbours[a2] = newChunk;
                }
                break;

        }
        chunk1.SetType();
        chunk2.SetType();
        if (newChunk != null)
        {
            newChunk.SetType();
            Debug.Log("added chunk" + newChunk.ID);
        }
        return true;
    }

    bool PointsAreClose (Vector2 point, Vector2 point2)
    {
        return ((point2 - point).sqrMagnitude < 0.2f);
    }

    /// <summary>
    /// Splits the chunk into two chunks using one of the two points (whichever is on the side, the other one must be in the corner) and modifies the relations.
    /// </summary>
    /// <returns><c>true</c>, if the chunk was split, <c>false</c> otherwise.</returns>
    /// <param name="chunk">The chunk to split.</param>
    /// <param name="point1">Point1.</param>
    /// <param name="point2">Point2.</param>
    /// <param name="neighbour">New neighbour from the other body.</param>
    bool SplitChunk (TerrainChunk chunk, Vector2 point1, Vector2 point2, TerrainChunk neighbour)
    {
        if (PointsAreClose(point1, point2))
            return false;
        Vector2[] corners = chunk.GetCorners();
        Vector2 point = Vector2.zero;
        bool cornerFound = false;
        //this is the corner that corresponds to either point, we need it to set the relations 
        Vector2 neighbourCorner = Vector2.zero;
        for (int i =0; i<3; i++)
        {
            //if one corner corresponds to one of two points, then choose the other point for division
            //and check whether the two points correspond to two corners
            if (PointsAreClose(corners[i], point1))
            {
                if (cornerFound)
                {
                    Debug.Log("both points are corners, only need to change neighbour");
                    return false;
                }
                point = point2;
                neighbourCorner = point1;
                cornerFound = true;
            }
            if (PointsAreClose(corners[i], point2))
            {
                if (cornerFound)
                {
                    Debug.Log("both points are corners, only need to change neighbour");
                    return false;
                }
                point = point1;
                neighbourCorner = point2;
                cornerFound = true;
            }
        }
        if (!cornerFound) return false;
        //now we check three sides and we have three cases
        for (int i = 0; i<3; i++)
        {
           //if the split point lie on this side of the triangle
           if (Vector3.Cross(point-corners[i], corners[(i+1)%3]-corners[i]).z < Mathf.Epsilon)
            {
                bool left = false;
                if (PointsAreClose(neighbourCorner, corners[i]))
                    left = true;
                else if (PointsAreClose(neighbourCorner, corners[(i + 1) % 3]))
                    left = false;
                else {
                    Debug.LogError("your logic wrong");
                    return false;
                }
                if (chunk.neighbours[i] != null)
                {
                    Debug.LogError("wrong again!");
                    return false;
                }
                TerrainChunk newChunk;
                //We reduce the chunk so that it will touch the other body and create the second half
                if (left)
                { 
                    chunk.SetCorners(new Vector2[3] { corners[i], point, corners[(i + 2) % 3] });
                    newChunk = AddChunk(new Vector2[3] { point, corners[(i + 1) % 3], corners[(i + 2) % 3] });

                    chunk.neighbours[i] = neighbour;
                    newChunk.neighbours[(i + 1) % 3] = chunk.neighbours[(i + 1) % 3];
                    chunk.neighbours[(i + 1) % 3] = newChunk;
                    newChunk.neighbours[(i + 2) % 3] = chunk;
                    if (newChunk.neighbours[(i + 1) % 3] != null)
                        newChunk.neighbours[(i + 1) % 3].ChangeNeighbour(newChunk, chunk);
                }
                else
                {
                    chunk.SetCorners(new Vector2[3] { point, corners[(i + 1) % 3], corners[(i + 2) % 3] });
                    newChunk = AddChunk(new Vector2[3] { corners[i], point, corners[(i + 2) % 3] });

                    chunk.neighbours[i] = neighbour;
                    newChunk.neighbours[(i + 2) % 3] = chunk.neighbours[(i + 2) % 3];
                    chunk.neighbours[(i + 2) % 3] = newChunk;
                    newChunk.neighbours[(i + 1) % 3] = chunk;
                    if (newChunk.neighbours[(i + 2) % 3]!=null)
                        newChunk.neighbours[(i + 2) % 3].ChangeNeighbour(newChunk, chunk);
                }
                //detect types
                chunk.SetType();
                if (newChunk != null)
                {
                    Debug.Log("added chunk" + newChunk.ID);
                    newChunk.SetType();
                }
                return true;
            }
            
        }
        //point is not on any side of the chunk
        return false;
    }



    /// <summary>
    /// Get the distance between the two specified chunks by using A* algorithm (currently just breadth-search).
    /// </summary>
    /// <returns>The shortest distance in chunks.</returns>
    /// <param name="a">First chunk.</param>
    /// <param name="b">Second chunk.</param>
    public int Distance(TerrainChunk a, TerrainChunk b) 
    {
        TerrainChunk[] path = ShortestPath(0, a, b);
        return path.Length-1;
    }

    /// <summary>
    /// A shortcut function that finds a path from chunk A to the nearest stable chunk.
    /// </summary>
    /// <returns>Path to the nearest stable chunk.</returns>
    /// <param name="a">Chunk A.</param>
    public TerrainChunk[] PathToNearestStable (TerrainChunk a)
    {
        return ShortestPath(1, a);
    }

    /// <summary>
    /// Returns the shortest path from chunk A to some other chunk, depending on the parameter k. Path includes the first and the last chunks.
    /// </summary>
    /// <returns>The shortest path.</returns>
    /// <param name="k">K = 0: shortest path to the specified chunk B. K = 1: shortest path to any stable chunk.</param>
    /// <param name="a">Chunk A.</param>
    /// <param name="b">Chunk B (required if k = 0).</param>
    public TerrainChunk[] ShortestPath (int k, TerrainChunk a, TerrainChunk b = null)
    {
        if (!Chunks.Contains(a) || (k==0 && !Chunks.Contains(b)))
            return null;
        if (k==0 && a == b) return new TerrainChunk[1] {b};
        if (k == 1 && a.stable) return null;

        TerrainChunk newChunk;
        //We use LinkedList here because it is optimal for insertions, and we don't need anything else from it
        LinkedList <TerrainChunk> openPaths = new LinkedList<TerrainChunk> ();
        openPaths.AddFirst(a);
        //We use HashSet here because we only need to check if the chunk is in the set (by its ID)
        HashSet<int> closedPaths = new HashSet<int> ();

        //In this dictionary we will store the tested chunk with its parent chunk, so that in the end we could find the whole path.
        Dictionary<TerrainChunk, TerrainChunk> steps = new Dictionary<TerrainChunk, TerrainChunk> ();


        while (true)
        {
            TerrainChunk currentChunk = openPaths.First.Value;
            TerrainChunk[] neighbours = currentChunk.neighbours;
            for (int i = 0; i < 3; i++)
            {
                newChunk = neighbours[i];
                if (newChunk != null &&
                    !closedPaths.Contains(newChunk.ID) &&
                    !openPaths.Contains(newChunk))
                {
                    if ((k == 0 && newChunk == b) || (k == 1 && newChunk.stable))
                    {
                        List<TerrainChunk> trianglePath = new List<TerrainChunk> { currentChunk, newChunk };
                        TerrainChunk parent;
                        TerrainChunk child = currentChunk;
                        while (child != a)
                        {
                            if (steps.TryGetValue(child, out parent))
                            {
                                trianglePath.Insert(0, parent);
                                child = parent;
                            }
                            else
                            {
                                Debug.LogError("can't find a chunk in the child-parent dictionary");
                                return null;
                            }
                        }

                        return trianglePath.ToArray();
                    }
                    //sort!
                    if (k == 1) 
                        InsertByStressAndDistance(newChunk, ref openPaths, firstStable);
                    else
                        openPaths.AddLast(newChunk);
                    steps.Add(newChunk, currentChunk);
                }
            }

            closedPaths.Add(currentChunk.ID);
            //visuals
            currentChunk.ColorChunk(Color.red);

            openPaths.Remove(currentChunk);
            if (openPaths.Count == 0)
            {
                //Debug.Log("Can't find the shortest path between " + a.GameChunk.name + " and " + b.GameChunk.name);
                return null;
            }
        }

    }

    private void InsertByStressAndDistance (TerrainChunk newchunk, ref LinkedList<TerrainChunk> chunks, Vector2 endpoint)
    {
        //maybe change to square but than we need to change stress factor as well
        //TODO check if we need the distance at all, if yes - then calculate from the real triangle points.
        if (endpoint!=Vector2.zero)
            newchunk.distToStable = (endpoint - newchunk.triangle.center).magnitude;

        //This is for the List, updated to LinkedList for performance
        //for (int i = 0; i<chunks.Count; i++)
        //{
        //    if (newchunk.Stress+newchunk.distToStable <= chunks[i].Stress+chunks[i].distToStable)
        //    {
        //        chunks.Insert(i, newchunk);
        //        return;
        //    }
        //}
        LinkedListNode<TerrainChunk> chunk = chunks.First;
        while (chunk != null)
        {
            if (newchunk.StaticStress + newchunk.distToStable <= chunk.Value.StaticStress + chunk.Value.distToStable)
            {
                chunks.AddBefore(chunk, newchunk);
                return;
            }
            chunk = chunk.Next;
        }
        chunks.AddLast(newchunk);
    }


    public void ColorTriangle(Color color, Triangle triangle)
    {
        GetChunkByTriangle(triangle).ColorChunk(color);
    }

    public TerrainChunk GetChunkByTriangle (Triangle triangle)
    {
        foreach (TerrainChunk chunk in Chunks)
        {
            if (chunk.triangle == triangle)
                return chunk;
        }
        return null;
    }
}
