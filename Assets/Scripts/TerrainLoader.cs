﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using GeometryMesh;
using System;
using System.Text.RegularExpressions;

public class TerrainLoader : MonoBehaviour {
    public TextAsset terrainFile;
    public int UpdateSpeed = 5;
    int fixedUpdateCounter = 0;
    
    MyTerrain myTerrain;

    // Use this for initialization
    void Start () {
        myTerrain = new MyTerrain();
        //read the data with saved meshes from json 
        SerialBodies savedMeshes = JsonUtility.FromJson<SerialBodies>(terrainFile.text);
        //put every saved mesh in a MyTerrain container
        for (int i = 0; i < savedMeshes.meshes.Length; i++)
        {
            GeometryMesh.Mesh newMesh = new GeometryMesh.Mesh(savedMeshes.meshes[i]);
            myTerrain.AddBody(newMesh);
        }
		myTerrain.UpdateBodies ();
	}

    //draw the meshes
    void OnDrawGizmos ()
    {
        if (myTerrain == null) return;

        foreach (Body body in myTerrain.Bodies)
        {
           
            foreach (TerrainChunk chunk in body.Chunks)
            {
                chunk.triangle.drawTriangle();
            }
        }
    }

    bool timeLogged = false;

    void Update ()
    {
        if (!timeLogged)
        {
            Debug.Log("time since startup: " + Time.realtimeSinceStartup);
            timeLogged = true;
        }

		//destroy chunk by click
		if (Input.GetMouseButtonDown (0)) {
			//DestroyChunkUnderMouse ();
			ApplyDynamicStressUnderMouse(50);
            }

    }

    void FixedUpdate()
    {
        fixedUpdateCounter++;
        if (fixedUpdateCounter == UpdateSpeed)
        {
            fixedUpdateCounter = 0;
            //destroy chunks and update
            myTerrain.UpdateBodies();
        }
    }

    //We can find the chunk 1) by coordinates, first determining all the nearby chunks and then running pointisinside
    //2) By raycasting - we'll use it first because it's easier
    void DestroyChunkUnderMouse () {
		Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);

		RaycastHit2D hitInfo = Physics2D.GetRayIntersection (mouseRay);
		//if something is found - get the body and chunk id and call the delete function
		if (hitInfo.collider!=null && hitInfo.collider.tag=="Terrain") 
		{
			int bodyID = GetIDFromName (hitInfo.collider.transform.parent.gameObject.name);
			int chunkID = GetIDFromName (hitInfo.collider.name);
			//Debug.Log ("Clicked on chunk "+chunkID+" in body "+bodyID);

			Body bodyUnderMouse = myTerrain.Bodies.Find (body => body.id == bodyID);
			bodyUnderMouse.DeleteChunk(chunkID);
			return;
		}
	}

	void ApplyDynamicStressUnderMouse (float amount) {
		Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);

		RaycastHit2D hitInfo = Physics2D.GetRayIntersection (mouseRay);
		//if something is found - get the body and chunk id and call the delete function
		if (hitInfo.collider!=null && hitInfo.collider.tag=="Terrain") 
		{
			int bodyID = GetIDFromName (hitInfo.collider.transform.parent.gameObject.name);
			int chunkID = GetIDFromName (hitInfo.collider.name);
			//Debug.Log ("Clicked on chunk "+chunkID+" in body "+bodyID);

			Body bodyUnderMouse = myTerrain.Bodies.Find (body => body.id == bodyID);
			bodyUnderMouse.ApplyDynamicStress(chunkID, amount);
			return;
		}
	}

	int GetIDFromName (string name) {
		Regex regex = new Regex (@"\d+");
		Match m = regex.Match (name);
		if (m.Success)
			return Convert.ToInt32(m.Value);
		else
			return -1;
	}
}
