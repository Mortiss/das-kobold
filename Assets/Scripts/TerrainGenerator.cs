﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class TerrainGenerator : MonoBehaviour {

	public Texture2D terrainSource;
	public float squareSize = 0.5f;
	public float chunkSize = 0.5f;

	[Space(10)]
	//for displaying the process of mesh generation
	public bool showMeshGeneration = false;
    public bool saveToFile = true;
    public string fileName = "test_terrain.txt";
     //this parameter is redundant, delete it later 
     float ImageRate = -1f;

	[Space(10)]
	public Material dirtMaterial;
	public float dirtStrength = 5f;
	public PhysicsMaterial2D dirtPhysics;
	public Material nondestMaterial;
//	public Material stoneMaterial;

	public static float minChunkSize, maxChunkSize;

	//white, red, black, grey
	public enum TerrainTypes { EMPTY, NONDEST, DIRT, STONE};
//	List<Body> dirtBodies, nondestBodies, stoneBodies;
	List<Body> bodies = new List<Body> ();

	SquareGrid squareGrid;
	List<Border> terrainBorders;
	int skip = 0, bodynum = 0;
	List<List<GeometryMesh.FrontVertex>> currentFronts;
	float nextTime;
	bool meshesComplete = false;
	//bool chunksCreated = false;

	void Start() {
		minChunkSize = chunkSize / 2f;
		maxChunkSize = chunkSize * 2f;
		if (showMeshGeneration)
			nextTime = Time.time+ImageRate;

		//Create the squares from bitmap
		squareGrid = new SquareGrid (terrainSource, squareSize);

		//Find borders on the square grid with the marching squares algorithm
		terrainBorders = CalculateBorders (squareGrid);

		//Combine borders into bodies (faces), finding outer borders and holes
		verifyAndCreateBodies (terrainBorders);

		//Generate non-uniform meshes from the bodies using advancing front algorythm
		if (bodies != null && !showMeshGeneration) {
			for (int i = 0; i < bodies.Count; i++) {
				bodies [i].MakeMesh (squareSize, out currentFronts);
				bodies [i].mesh.ContinueGenerating (currentFronts);
			}
			meshesComplete = true;
		}

        //we save meshes to json for now, change it to binary later
		if (saveToFile && meshesComplete)
        {
            //Create a container object: we can't serialize without it. Maybe we shall change it later.
            GeometryMesh.SerialBodies serialMeshes = new GeometryMesh.SerialBodies(bodies);

            string meshData = JsonUtility.ToJson(serialMeshes);
            using (StreamWriter writer = File.CreateText(fileName))
            {
                writer.Write(meshData);
            }

        }
        //			CreateChunks ();
    }



    //Create physical chunks from meshed bodies and tie them up with joints
 //   void CreateChunks () {
	//	GameObject terrain = new GameObject ();
	//	terrain.name = "Terrain";
	//	foreach (Body body in bodies) {
	//		if (body.getTerrainType () == TerrainTypes.DIRT) {
	//			GameObject dirt = body.mesh.MeshToChunks (dirtMaterial, dirtStrength, dirtPhysics);
	//			dirt.name = "body" + bodies.IndexOf (body).ToString ()+" dirt";
	//			dirt.transform.parent = terrain.transform;
	//		}
	//		if (body.getTerrainType () == TerrainTypes.NONDEST) {
	//			GameObject nondest = new GameObject ();
	//			MeshFilter mf = nondest.AddComponent<MeshFilter> ();
	//			mf.mesh = body.mesh.ToUnityMesh ();
	//			MeshRenderer mr = nondest.AddComponent<MeshRenderer> ();
	//			mr.material = nondestMaterial;
	//			PolygonCollider2D col = nondest.AddComponent<PolygonCollider2D> ();
	//			col.points = body.simpleBorder.polygon.ToArray();
	//			Rigidbody2D rb = nondest.AddComponent<Rigidbody2D> ();
	//			rb.useAutoMass = true;
	//			nondest.name = "body" + bodies.IndexOf (body).ToString ()+" nondest";
	//			nondest.transform.parent = terrain.transform;
	//		}
	//	}
	//	chunksCreated = true;

	//	//create joints between different bodies here
	//	terrain.BroadcastMessage ("FindOtherNeighbours", SendMessageOptions.DontRequireReceiver);
	//}

	void Update () {
		if (bodies == null || !showMeshGeneration )
			return;

		//this here code is for displaying the mesh generation process when showMesheneration = true
		if (!meshesComplete && Time.time > nextTime) {
			if (bodies [bodynum].mesh == null)
				bodies [bodynum].MakeMesh (squareSize, out currentFronts);
			GeometryMesh.Mesh mesh = bodies [bodynum].mesh;

			if (currentFronts [0].Count > 4) {
				if (mesh.GenerateAMeshTriangle (currentFronts, currentFronts [0] [skip])) {
					skip = 0;
					currentFronts [0].Sort (GeometryMesh.FrontVertex.CompareByAngle);
				} else
					skip++;
				if (skip == currentFronts [0].Count) {
					Debug.Log ("Bad Front");
					foreach (GeometryMesh.FrontVertex v in currentFronts[0])
						Debug.Log (v); 
					throw new UnityException ("Couldn't triangulate this border!");
				}
			} else if (currentFronts [0].Count == 4) {
				mesh.GenerateLastMeshTriangles (currentFronts [0]);
				skip = 0;
				currentFronts.RemoveAt (0);
				if (currentFronts.Count == 0) {
					mesh.CalculateNeighbours ();
					bodynum++;
					if (bodynum == bodies.Count)
						meshesComplete = true;
					Debug.Log ("mesh created");
				}
			}
			nextTime += ImageRate;
		}

		//if (meshesComplete && !chunksCreated)
			//CreateChunks ();
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.white;
//		if (terrainBorders != null) {
//			foreach (Border border in terrainBorders)
//				border.drawBorder ();
//		}
			
		if (showMeshGeneration && bodies != null && bodynum < bodies.Count) {
			foreach (Body body in bodies) {
				if (body.simpleBorder != null)
					body.simpleBorder.drawBorder ();
				if (body.mesh != null)
					body.mesh.drawGizmos ();
			}
		}
		Gizmos.color = Color.red;
		if (currentFronts != null) {
			foreach (List<GeometryMesh.FrontVertex> front in currentFronts)
				foreach (GeometryMesh.FrontVertex v in front)
					v.drawForward ();
		}
	}

	void verifyAndCreateBodies (List <Border> borders) {
		Body body;
//		List <Body> bodyList; 
		foreach (Border border in borders) {
			if (!border.isHole) {
				body = new Body (border);
//				bodyList = getContainer (body.getTerrainType ());
//				if (bodyList!=null)
//					bodyList.Add (body); 
				bodies.Add (body);
			}
		}
		foreach (Border border in borders) {
			if (border.isHole) {
//				bodyList = getContainer (border.getType ());
//				if (bodyList == null) {
//					Debug.Log ("This hole doesn't have a parent: " + borders.IndexOf (border));
//					continue;
//				}
				List<Body> parentBodyList = new List <Body> ();
				foreach (Body parentBody in bodies) {
					if (parentBody.getTerrainType () == border.getType () && parentBody.pointIsInside (border.getFirstVertex ())) {
						parentBodyList.Add (parentBody);
					}
				}
				if (parentBodyList.Count == 0) {
					Debug.Log ("This hole doesn't have a parent: " + borders.IndexOf (border));
					continue;
				}
				int j = 0;
				int initialCount = parentBodyList.Count;
				while (parentBodyList.Count > 1) {
					j++;
					for (int i = 1; i < parentBodyList.Count; i++) {
						if (parentBodyList [i].IsInside (parentBodyList [0]))
							parentBodyList.RemoveAt (0);
						else if (parentBodyList [0].IsInside (parentBodyList [i])) {
							Body tempBody = parentBodyList [0];
							parentBodyList [0] = parentBodyList [i];
							parentBodyList [i] = tempBody;
						}	
					}
					if (j > initialCount*initialCount) {
						Debug.Log ("Can't find a parent for this hole: " + borders.IndexOf (border));
						break;
					}
				}
				if (parentBodyList.Count == 1) {
					parentBodyList [0].addHole (border);
					Debug.Log ("new hole in body: " + bodies.IndexOf (parentBodyList [0]) + ", type " + border.getType ());
				}
			}
		}
	}

/*	List<Body> getContainer (terrainTypes type) {
		switch (type) {
		case terrainTypes.DIRT:
			if (dirtBodies == null)
				dirtBodies = new List <Body> ();
			return dirtBodies;
		case terrainTypes.NONDEST:
			if (nondestBodies == null)
				nondestBodies = new List <Body> ();
			return nondestBodies;
		case terrainTypes.STONE:
			if (stoneBodies == null)
				stoneBodies = new List <Body> ();
			return stoneBodies;
		default:
			return null;
		}
	}*/

	List<Border> CalculateBorders(SquareGrid squareGrid) {
		List<Border> borders = new List<Border> ();
		Border border;
		int xCount = squareGrid.squares.GetLength (0);
		int yCount = squareGrid.squares.GetLength (1);

		Debug.Log ("Started calculating borders...");

		//creating outer border with type of the first controlNode
		if (squareGrid.squares[0,0].bottomLeft.terrainType!= TerrainTypes.NONDEST)
			Debug.Log ("Warning! Outer border is destructable.");
		border = new Border (squareGrid.squares[0,0].bottomLeft.terrainType);
		border.addEdge (squareGrid.squares [0, 0].bottomLeft.position, squareGrid.squares [0, yCount-1].topLeft.position);
		border.addEdge (squareGrid.squares [0, yCount-1].topLeft.position, squareGrid.squares [xCount-1, yCount-1].topRight.position);
		border.addEdge (squareGrid.squares [xCount-1, yCount-1].topRight.position, squareGrid.squares [xCount-1, 0].bottomRight.position);
		border.addEdge (squareGrid.squares [xCount-1, 0].bottomRight.position, squareGrid.squares [0, 0].bottomLeft.position);
		border.CheckOutBorder ();
		borders.Add (border);

		foreach (TerrainTypes i in Enum.GetValues(typeof(TerrainTypes))) {
			if (i == TerrainTypes.EMPTY)
				continue;
			for (int x = 0; x < xCount; x++) {
				for (int y = 0; y < yCount; y++) {
					if (squareGrid.squares [x, y].bordersFound)
						continue;
					if (squareGrid.squares [x, y].checkConfiguration (i) == 0 || squareGrid.squares [x, y].checkConfiguration (i) == 15)
						continue;
					border = new Border (i);
					int nextX = x, nextY = y;
					int[] next;
					for (int j = 0; j < xCount * yCount; j++) {
						next = squareGrid.squares[nextX,nextY].findBorder(border, i);
						if (next == new int[2] {0,0}) {
							Debug.Log ("Problem drawing border @ square "+next);
						}
						nextX += next[0];
						nextY += next[1];
						if ((nextX > xCount - 1) || (nextX < 0) || (nextY > yCount - 1) || (nextY < 0))
							throw new UnityException ("Error! Map must be enclosed in an object of some type!");
						if (nextX == x && nextY == y) {
							Debug.Log (j);
							break;
						}
					}

					border.CheckOutBorder ();
					borders.Add (border);
				}
			}
			squareGrid.clearFlags ();
		}
		Debug.Log ("done!");
		Debug.Log (borders.Count);
		return borders;
	}

	public class Border {
		public List<Vector2> polygon;
		public TerrainTypes objectType;
		public bool isHole;
		private int angleSum;
		private Vector2 vertexMaxX, vertexMinX, vertexMaxY, vertexMinY;
	//	public Vertex[] vertices;

		public Border (TerrainTypes _objectType) {
			polygon = new List<Vector2> ();
			objectType=_objectType;
			angleSum = 0;
		}

		public Border (Border _border) {
			objectType = _border.objectType;
			isHole = _border.isHole;
			angleSum = _border.angleSum;
			vertexMaxX = _border.vertexMaxX;
			vertexMinX = _border.vertexMinX;
			vertexMaxY = _border.vertexMaxY;
			vertexMinY = _border.vertexMinY;
			polygon = new List<Vector2> ();
			for (int i =0; i<_border.polygon.Count; i++)
				polygon.Add (_border.polygon[i]);
		}

//		public void makeVertices () {
//			vertices = new Vertex[polygon.Count - 1];
//
//			for (int i = 0; i < polygon.Count - 1; i++) {
//				vertices [i] = new Vertex (polygon [i]);
//				vertices [i].isBorder = true;
//			}
//			for (int i = 0; i < polygon.Count - 1; i++) {
//				if (i==0)
//					vertices [i].setNeighbours (vertices [polygon.Count - 2], vertices [i+1]);
//				else if (i==polygon.Count - 2)
//					vertices [i].setNeighbours (vertices [i - 1], vertices [0]);
//				else
//					vertices [i].setNeighbours (vertices [i - 1], vertices [i + 1]);
//			}
//							
//		}

		public Vector2[] getBounds() {
			return new Vector2[4] {vertexMaxX, vertexMinX, vertexMaxY, vertexMinY};
		}

		public TerrainTypes getType() {
			return objectType;
		}

		public void setType (TerrainTypes _objectType) {
			objectType = _objectType;
		}

		public void CheckOutBorder () {
			int lastIndex = polygon.Count-1;
			changeAngleSum (polygon[lastIndex] - polygon [lastIndex-1], polygon[1] - polygon [0]);
			if (angleSum == -360)
			{
				isHole = true;
				Debug.Log ("Polygon created, and it is a hole");
			}
			else if (angleSum == 360)
			{
				isHole = false;
				Debug.Log ("Polygon created");
			}
			else
				Debug.Log ("Polygon is not a loop!!!"+angleSum);
		}

		public bool sameAsLast (Vector2 vertex) {
			if (polygon.Count < 2)
				return false;
			return (vertex == polygon [polygon.Count - 1]);
		}

		public int getIndex (Vector2 vertex) {
			return polygon.IndexOf (vertex);
		}

		public Intersection FindNearestIntersectionToTheRight (Vector2 point) {
			Intersection closestIntersection = new Intersection (); 

			Intersection newIntersection; 
			Vector2 intersectionPosition;
			for (int i = 0, j = 1; j < polygon.Count; i = j++) {
				if ((polygon [i].y <= point.y) != (polygon [j].y <= point.y)) {
					intersectionPosition = new Vector2 (polygon[i].x + (polygon[j].x - polygon[i].x) * (point.y - polygon[i].y) / (polygon[j].y - polygon[i].y), point.y);
					if (intersectionPosition.x <= point.x)
						continue;
					newIntersection = new Intersection (intersectionPosition, this, polygon[i], polygon[j], i);
					if (closestIntersection.FartherThan (newIntersection))
						closestIntersection = newIntersection;
				}
			}
			return closestIntersection;
		}

		//make additional vertices for mesh creation
		public Border addVerticesForMesh (float chunkSize) {			
			Vector2 edge;
			int verticesCount;
			Border newPolygon = new Border (this);
			int addedVerticesCount = 0;
			float maxChunkSize = chunkSize * 2f;
			float minChunkSize = chunkSize / 2f;

			//for every edge check to see whether its magnitude is longer then maxChunkSize
			for (int i = 0, j = 1; j < polygon.Count; i = j++) {
				edge = polygon [j] - polygon [i];
				if (edge.magnitude > maxChunkSize) {
					//determine number of vertices to add
					if (edge.magnitude % maxChunkSize < minChunkSize)
						verticesCount = (int)(edge.magnitude / maxChunkSize) - 1;
					else 
						verticesCount = (int)(edge.magnitude / maxChunkSize - edge.magnitude % maxChunkSize);

					//add 1 to make an even number
//					if (verticesCount % 2 == 1)
//						verticesCount++;

//					//divide the edge into even segments
					float segment = edge.magnitude/(verticesCount+1);
					
					//add vertices to this edge in the new polygon
					for (int k = 0; k < verticesCount; k++) {
						newPolygon.addVertexAfter (polygon [i] + edge.normalized * segment * (k+1), i+k+addedVerticesCount);

					}
					addedVerticesCount+=verticesCount;
				}
			}
			return newPolygon;
		}

		public void addVertexAfter (Vector2 vertexToAdd, int vertexOfBorder) {
			polygon.Insert (vertexOfBorder+1, vertexToAdd);
		}

		public void addEdge (Vector2 vertex1, Vector2 vertex2) {
			//first edge
			if (polygon.Count < 2) {
				polygon.Add (vertex1);
				polygon.Add (vertex2);
				if (vertex1.x > vertex2.x) {
					vertexMaxX = vertex1;
					vertexMinX = vertex2;
				}
				else {
					vertexMaxX = vertex2;
					vertexMinX = vertex1;
				}
				if (vertex1.y > vertex2.y) {
					vertexMaxY = vertex1;
					vertexMinY = vertex2;
				} else {
					vertexMaxY = vertex2;
					vertexMinY = vertex1;
				}
			}

			else {
				int lastIndex = polygon.Count-1;

				if (!sameAsLast(vertex1)) {
					Debug.Log ("Something went wrong when calculating borders!");
				}

				int angle = (int)Vector2.Angle (polygon [lastIndex] - polygon [lastIndex - 1], vertex2 - polygon [lastIndex]);
				if (angle == 0)
					polygon.RemoveAt (lastIndex);
				else 
					changeAngleSum (polygon [lastIndex] - polygon [lastIndex - 1], vertex2 - polygon [lastIndex]);

				polygon.Add(vertex2);
				if (vertex2.x > vertexMaxX.x)
					vertexMaxX = vertex2;
				if (vertex2.x < vertexMinX.x)
					vertexMinX = vertex2;
				if (vertex2.y > vertexMaxY.y)
					vertexMaxY = vertex2;
				if (vertex2.y < vertexMinY.y)
					vertexMinY = vertex2;
			}
		}

		void changeAngleSum (Vector2 edge1, Vector2 edge2) {
			int angle = (int)Vector2.Angle (edge1, edge2);
			Vector3 cross = Vector3.Cross (edge1, edge2);
			if (cross.z < 0)
				angleSum = angleSum + angle;
			else 
				angleSum = angleSum - angle;
		}

		public void drawBorder () {
			for (int k = 0; k < polygon.Count - 1; k++) {
				Gizmos.DrawWireSphere (polygon [k], .1f);
				Gizmos.DrawLine (polygon [k], polygon [k + 1]);
			}
		}

		public Vector2 getFirstVertex () {
			return polygon [0];
		}

		public bool pointIsInside (Vector2 point) {
			int crossingsCount = 0;
			for (int i = 0, j = 1; j < polygon.Count; i = j++) {
				if (((polygon [i].x <= point.x) != (polygon [j].x <= point.x)) ||
					((polygon [i].x == point.x) && (polygon [i].x < polygon[j].x)) || ((polygon [j].x == point.x) && (polygon [j].x < polygon[i].x)))
				{
					if (polygon[i].y + (point.x - polygon[i].x) / (polygon[j].x - polygon[i].x) * (polygon[j].y - polygon[i].y) < point.y)
						crossingsCount++;
				} 
			}
			if (crossingsCount % 2 == 0)
				return false;
			else
				return true;
		}

		//make connection with another border between designated points and add all its vertices to this one
		public Border connectWith (Border innerBorder, Vector2 startPoint, Vector2 endPoint) {
			Border unifiedBorder = new Border (this);
			int k = innerBorder.polygon.IndexOf (endPoint);
			List<Vector2> insertion = new List<Vector2> ();
			for (int i = k; i < innerBorder.polygon.Count; i++)
				insertion.Add (innerBorder.polygon [i]);
			for (int i = 1; i <= k; i++)
				insertion.Add (innerBorder.polygon [i]);
			insertion.Add (startPoint);
			unifiedBorder.polygon.InsertRange (polygon.IndexOf (startPoint) + 1, insertion);
			return unifiedBorder;
		}
	}

	public class SquareGrid {
		public Square[,] squares;
		int nodeCountX, nodeCountY;

		public SquareGrid(Texture2D terrainSource, float squareSize) {
			nodeCountX = terrainSource.width;
			nodeCountY = terrainSource.height;
			float mapWidth = nodeCountX * squareSize;
			float mapHeight = nodeCountY * squareSize;

			ControlNode[,] controlNodes = new ControlNode[nodeCountX,nodeCountY];

			for (int x = 0; x < nodeCountX; x ++) {
				for (int y = 0; y < nodeCountY; y ++) {
					Vector3 pos = new Vector3(-mapWidth/2 + x * squareSize + squareSize/2, -mapHeight/2 + y * squareSize + squareSize/2, 0);
                    TerrainTypes terrainType;
					if (terrainSource.GetPixel(x,y) == Color.black) 
							terrainType = TerrainTypes.DIRT;
					else if (terrainSource.GetPixel(x,y) ==Color.red)
							terrainType = TerrainTypes.NONDEST;
					else if (terrainSource.GetPixel(x,y) ==Color.gray)
							terrainType = TerrainTypes.STONE;
					else terrainType = TerrainTypes.EMPTY;
					controlNodes[x,y] = new ControlNode(pos, terrainType, squareSize);
				}
			}

			squares = new Square[nodeCountX -1,nodeCountY -1];
			for (int x = 0; x < nodeCountX-1; x ++) {
				for (int y = 0; y < nodeCountY-1; y ++) {
					squares[x,y] = new Square(controlNodes[x,y+1], controlNodes[x+1,y+1], controlNodes[x+1,y], controlNodes[x,y]);
				}
			}
		}

		public void clearFlags() {
			for (int x = 0; x < nodeCountX - 1; x++) {
				for (int y = 0; y < nodeCountY - 1; y++) {
					squares [x, y].bordersFound = false;
					squares [x, y].numBorderCounted = 0;
				}
			}
		}
	}

	public class Square {

		public ControlNode topLeft, topRight, bottomRight, bottomLeft;
		public Node centreTop, centreRight, centreBottom, centreLeft;
		public bool bordersFound = false;
		public int numBorderCounted = 0;
		public int numBorders = 0;

		public Square (ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomRight, ControlNode _bottomLeft) {
			topLeft = _topLeft;
			topRight = _topRight;
			bottomRight = _bottomRight;
			bottomLeft = _bottomLeft;

			centreTop = topLeft.right;
			centreRight = bottomRight.above;
			centreBottom = bottomLeft.right;
			centreLeft = bottomLeft.above;
		}

		//k=0 for bottom left node, and it goes clockwise
		// even numbers for control nodes
		public TerrainTypes getCNodeType (int k) {
			switch (k % 8) {
			case 0:
				return bottomLeft.terrainType;
			case 2:
				return topLeft.terrainType;
			case 4:
				return topRight.terrainType;
			case 6:
				return bottomRight.terrainType;
			default:
				return TerrainTypes.EMPTY;
			}
		}

		public Vector2 getNodePosition (int k) {
			switch (k % 8) {
			case 0:
				return bottomLeft.position;
			case 1:
				return centreLeft.position;
			case 2:
				return topLeft.position;
			case 3:
				return centreTop.position;
			case 4:
				return topRight.position;
			case 5:
				return centreRight.position;
			case 6:
				return bottomRight.position;
			case 7:
				return centreBottom.position;
			default:
				return Vector2.one;
			}
		}

		private void findSpecialBorder (Border border, int node) {
			if (numBorders == 3) {
				border.addEdge (getNodePosition (node + 1), getNodePosition (node + 3));
				border.addEdge (getNodePosition (node + 3), getNodePosition (node + 5));
				border.addEdge (getNodePosition (node + 5), getNodePosition (node + 7));
			} else if (numBorders == 2) {
				if (getCNodeType (node + 4) == getCNodeType (node + 6)) {
					border.addEdge (getNodePosition (node + 1), getNodePosition (node + 3));
					border.addEdge (getNodePosition (node + 3), getNodePosition (node + 7));
				} else if (getCNodeType (node + 2) == getCNodeType (node + 4)) {
					border.addEdge (getNodePosition (node + 1), getNodePosition (node + 5));
					border.addEdge (getNodePosition (node + 5), getNodePosition (node + 7));
				}
			} else if (numBorders < 2)
				border.addEdge (getNodePosition (node + 1), getNodePosition (node + 7));
		}

		public int [] findBorder (Border border, TerrainTypes i) {
			int nextX = 0, nextY = 0;
			bool markAsComplete = true;
			switch (checkConfiguration (i)) {  
			case 1:
				findSpecialBorder (border, 0);
				nextY = nextY - 1;
				break;
			case 2:
				findSpecialBorder (border, 6);
				nextX = nextX + 1;
				break;
			case 3:
				if (numBorders == 2) {
					border.addEdge (centreLeft.position, centreTop.position);
					border.addEdge (centreTop.position, centreRight.position);
				} else
					border.addEdge (centreLeft.position, centreRight.position);
				nextX = nextX + 1;
				break;
			case 4:
				findSpecialBorder (border, 4);
				nextY = nextY + 1;
				break;
			case 5:  
				if (numBorderCounted == 0) {							
					markAsComplete = false;
				}
				if (border.sameAsLast (centreLeft.position)) {
					border.addEdge (centreLeft.position, centreTop.position);
					numBorderCounted = 1;
					nextY = nextY + 1;
				} else if (border.sameAsLast (centreRight.position)) {
					border.addEdge (centreRight.position, centreBottom.position);
					numBorderCounted = 2;
					nextY = nextY - 1;
				} else {
					if (numBorderCounted == 1) {
						border.addEdge (centreRight.position, centreBottom.position);
						nextY = nextY - 1;
					} else if (numBorderCounted == 2 || numBorderCounted == 0) {
						border.addEdge (centreLeft.position, centreTop.position);
						nextY = nextY + 1;
					}
				}
				break;
			case 7:
				border.addEdge (centreLeft.position, centreTop.position);
				nextY = nextY + 1;
				break;
			case 6:
				if (numBorders == 2) {
					border.addEdge (centreBottom.position, centreLeft.position);
					border.addEdge (centreLeft.position, centreTop.position);
				} else
					border.addEdge (centreBottom.position, centreTop.position);
				nextY = nextY + 1;
				break;
			case 8:
				findSpecialBorder (border, 2);
				nextX = nextX - 1;
				break;
			case 9:
				if (numBorders == 2) {
					border.addEdge (centreTop.position, centreRight.position);
					border.addEdge (centreRight.position, centreBottom.position);
				} else
					border.addEdge (centreTop.position, centreBottom.position);
				nextY = nextY - 1;
				break;
			case 10: 
				if (numBorderCounted == 0) {							
					markAsComplete = false;
				}
				if (border.sameAsLast (centreTop.position)) {
					border.addEdge (centreTop.position, centreRight.position);
					numBorderCounted = 1;
					nextX = nextX + 1;
				} else if (border.sameAsLast (centreBottom.position)) {
					border.addEdge (centreBottom.position, centreLeft.position);
					numBorderCounted = 2;
					nextX = nextX - 1;
				} else {
					if (numBorderCounted == 1) {
						border.addEdge (centreBottom.position, centreLeft.position);
						nextX = nextX - 1;
					} else if (numBorderCounted == 2 || numBorderCounted == 0) {
						border.addEdge (centreTop.position, centreRight.position);
						nextX = nextX + 1;
					}
				}
				break;
			case 14:
				border.addEdge (centreBottom.position, centreLeft.position);
				nextX = nextX - 1;
				break;
			case 11:
				border.addEdge (centreTop.position, centreRight.position);
				nextX = nextX + 1;
				break;
			case 12:
				if (numBorders == 2) {
					border.addEdge (centreRight.position, centreBottom.position);
					border.addEdge (centreBottom.position, centreLeft.position);
				} else
					border.addEdge (centreRight.position, centreLeft.position);
				nextX = nextX - 1;
				break;
			case 13:
				border.addEdge (centreRight.position, centreBottom.position);
				nextY = nextY - 1;
				break;
			}
			int [] direction = new int[2] {nextX, nextY};
			if (direction != new int[2] {0,0} && markAsComplete)
			{
				bordersFound = true;
				numBorders++;
			}
			return direction;
		}

		public int checkConfiguration (TerrainTypes terrainType) {
			int configuration = 0;
			if (topLeft.terrainType==terrainType)
				configuration += 8;
			if (topRight.terrainType==terrainType)
				configuration += 4;
			if (bottomRight.terrainType==terrainType)
				configuration += 2;
			if (bottomLeft.terrainType==terrainType)
				configuration += 1;
			return configuration;
		}
	}

	public class Node {
		public Vector3 position;

		public Node(Vector3 _pos) {
			position = _pos;
		}
	}

	public class ControlNode : Node {

		public TerrainTypes terrainType;
		public Node above, right;

		public ControlNode(Vector3 _pos, TerrainTypes _terrainType, float squareSize) : base(_pos) {
			terrainType = _terrainType;
			above = new Node(position + Vector3.up * squareSize/2f);
			right = new Node(position + Vector3.right * squareSize/2f);
		}
	
	}

	public class Body {
		public Border outBorder, simpleBorder;
		private TerrainTypes bodyType;
		private List<Border> holes;
		public GeometryMesh.Mesh mesh;

		public Body (Border _outBorder) {
			outBorder = _outBorder;
			bodyType = _outBorder.getType ();
			holes = new List<Border> ();
		}

		public void drawOutBorder () {
			outBorder.drawBorder ();
		}

		public bool pointIsInside (Vector2 point) {
			return outBorder.pointIsInside (point);
		}

		public bool IsInside (Body body2) {
			return body2.pointIsInside (outBorder.getFirstVertex());
		}

//		//make additional vertexes on the outer border for use in mesh creation
//		public void addVerticesForMeshCreation () {
//			outBorder = outBorder.addVerticesForMesh ();
//		}

		public void MakeMesh (float chunkSize, out List<List<GeometryMesh.FrontVertex>> fronts) {
			if (simpleBorder==null) {
				simplify ();
				simpleBorder = simpleBorder.addVerticesForMesh (chunkSize);
			}
			fronts = new List<List<GeometryMesh.FrontVertex>> ();
			fronts.Add (new List<GeometryMesh.FrontVertex> ());
			mesh = new GeometryMesh.Mesh (simpleBorder.polygon, chunkSize, fronts);
		}

		//connect holes with outer border
		private void simplify() {
			List<Border> borders = new List<Border> ();
			foreach (Border hole in holes)
				borders.Add (hole);
			borders.Add (outBorder);

			Intersection nearestIntersection, newIntersection;

			for (int i = 0; i<holes.Count; i++) {
				Vector2 vertexWithMaxX = borders[i].getBounds () [0];

				nearestIntersection = new Intersection ();
				//find the nearest border (must exist)
				for (int k = i + 1; k < borders.Count; k++) {
					newIntersection = borders [k].FindNearestIntersectionToTheRight (vertexWithMaxX);
					if (nearestIntersection.FartherThan (newIntersection))
						nearestIntersection = newIntersection;
				}
				if (!nearestIntersection.DefaultIntrsc()) {
					//check against nearest vertices
					if ((nearestIntersection.previousVertex - nearestIntersection.position).magnitude < minChunkSize)
						nearestIntersection.position = nearestIntersection.previousVertex;
					else if ((nearestIntersection.nextVertex - nearestIntersection.position).magnitude < minChunkSize)
						nearestIntersection.position = nearestIntersection.nextVertex;
					else
						nearestIntersection.parent.addVertexAfter (nearestIntersection.position, nearestIntersection.prevVertexIndex);

					//add this border to it
					int parentIndex = borders.IndexOf(nearestIntersection.parent);
					borders[parentIndex] = nearestIntersection.parent.connectWith (borders[i], nearestIntersection.position, vertexWithMaxX);
				}

			}
			simpleBorder = borders[borders.Count-1];
		}

		public TerrainTypes getTerrainType () {
			return bodyType;
		}

		public void addHole (Border hole) {
			holes.Add (hole);
		}
	
	}

	public class Intersection {
		public Vector2 position;
		public Border parent;
		public Vector2 previousVertex, nextVertex;
		public int prevVertexIndex;

		public Intersection () {
			position = new Vector2 (1e6f, 1e6f);
		}

		public Intersection (Vector2 _position, Border _parent, Vector2 _previousVertex, Vector2 _nextVertex, int _prevVertexIndex) {
			parent = _parent;
			position = _position;
			previousVertex = _previousVertex;
			nextVertex = _nextVertex;
			prevVertexIndex = _prevVertexIndex;
			}

		public bool FartherThan (Intersection intersection2) {
			if (intersection2 != null)
				return (this.position.x > intersection2.position.x);
			else
				return false;
		}

		public bool DefaultIntrsc () {
			return (position == new Vector2 (1e6f, 1e6f));
		}
	}


}
