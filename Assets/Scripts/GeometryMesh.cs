﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GeometryMesh
{
    /// <summary>
    /// A container for several SerializableMeshes. We can't serialize them without a container object.
    /// </summary>
    [Serializable]
    public class SerialBodies
    {
        public GeometryMesh.SerializableMesh[] meshes;

        public SerialBodies(List<TerrainGenerator.Body> bodies)
        {
            PrepareForSerialization(bodies);
        }

        void PrepareForSerialization(List<TerrainGenerator.Body> bodies)
        {
            meshes = new GeometryMesh.SerializableMesh[bodies.Count];
            for (int i = 0; i < bodies.Count; i++)
            {
                meshes[i] = bodies[i].mesh.serializableMesh;
            }
        }
    }
    /// <summary>
    /// A structure for Physical Mesh that you can save to file or use with other serialization needs.
    /// </summary>
    [Serializable]
    public class SerializableMesh
    {
        public SerialVertex[] vertices;

        public SerialTriangle[] triangles;

        public SerializableMesh(Mesh mesh)
        {
            vertices = new SerialVertex[mesh.Vertices.Count];
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = new SerialVertex(mesh.Vertices[i]);
            }

            triangles = new SerialTriangle[mesh.Triangles.Count];
            for (int i = 0; i < triangles.Length; i++)
            {
                triangles[i] = new SerialTriangle(mesh.Triangles[i], mesh.Vertices);
            }
            Debug.Log("creating data for serialization... "+vertices.Length+" "+triangles.Length);
        }

    }

    public class Mesh
    {
        /// <summary>
        /// Gets triangles of the mesh (read-only).
        /// </summary>
        /// <value>The triangles.</value>
        public List<Triangle> Triangles { get; private set; }
        /// <summary>
        /// Gets vertices of the mesh (read-only).
        /// </summary>
        /// <value>The vertices.</value>
        public List<Vertex> Vertices { get; private set; }

        float maxChunkSize, minChunkSize;

        internal const float EPSILON = 1E-7f;
        int snappedIndex = -1;
        //	int casenum;

       
        /// <summary>
        /// A version of Physical Mesh that you can save to file or use with other serialization needs.
        /// </summary>
        public SerializableMesh serializableMesh;

        /// <summary>
        /// Loads the mesh from a previously saved serializable mesh
        /// </summary>
        /// <param name="sMesh">The serializable mesh.</param>
        public Mesh (SerializableMesh sMesh)
        {
            serializableMesh = sMesh;
            //fill in the fields
            Vertices = new List<Vertex>();
            for (int i = 0; i<sMesh.vertices.Length; i++)
            {
                Vertex v = new Vertex(sMesh.vertices[i]);
                Vertices.Add(v);
            }

            Triangles = new List<Triangle>();
            for (int i = 0; i < sMesh.triangles.Length; i++)
            {
                SerialTriangle currentTriangle = sMesh.triangles[i];
                Triangle abc = new Triangle(Vertices[currentTriangle.v1], 
                                            Vertices[currentTriangle.v2], 
                                            Vertices[currentTriangle.v3]);
                Triangles.Add(abc);
            }
            CalculateNeighbours();
            Debug.Log("mesh loaded from file");
        }

		/// <summary>
		/// Recreats a geometry mesh object from a list of physical chunks (used when dividing physical bodies)
		/// </summary>
		/// <param name="listOfChunks">List of chunks.</param>
		public Mesh (List<TerrainChunk> listOfChunks) 
		{
			Vertices = new List<Vertex>(listOfChunks.Count*3);
			Triangles = new List<Triangle>(listOfChunks.Count);

			foreach (TerrainChunk chunk in listOfChunks) {
				Triangles.Add (chunk.triangle);
				Vertex[] newVertices = { chunk.triangle.v1, chunk.triangle.v2, chunk.triangle.v3 };
				foreach (Vertex v in newVertices)
					if (!Vertices.Contains (v))
						Vertices.Add (v);
			}
			Vertices.TrimExcess ();
		}

        /// <summary>
        /// Creates a new physical mesh by triangulating a polygon using advancing front algorithm.
        /// </summary>
        /// <param name="polygon">List of points describing a non-self-intersecting polygon (last point=first point), points must go clockwise.</param>
        /// <param name="chunkSize">Average size of triangles.</param>
        public Mesh(List<Vector2> polygon, float chunkSize, List<List<FrontVertex>> fronts)
        {
            Vertices = new List<Vertex>();
            Triangles = new List<Triangle>();
            maxChunkSize = 2f * chunkSize;
            minChunkSize = chunkSize / 2f;
            GenerateMesh(polygon, fronts);
        }

        public void drawGizmos()
        {
            foreach (Triangle tri in Triangles)
                tri.drawTriangle();
        }

        /// <summary>
        /// Does some stuff after generating the mesh. Currently calculates neighbouring triangles.
        /// </summary>
        public void CalculateNeighbours()
        {
            for (int i = 0; i < Triangles.Count; i++)
            {

                foreach (Triangle atv1 in Triangles[i].v1.adjacentTriangles)
                {
                    if (Triangles[i].v2.adjacentTriangles.Contains(atv1) && atv1 != Triangles[i])
                    {
                        Triangles[i].adjab = atv1;
                        Triangles[i].numNeighbours++;
                        break;
                    }
                }

                foreach (Triangle atv2 in Triangles[i].v2.adjacentTriangles)
                {
                    if (Triangles[i].v3.adjacentTriangles.Contains(atv2) && atv2 != Triangles[i])
                    {
                        Triangles[i].adjbc = atv2;
                        Triangles[i].numNeighbours++;
                        break;
                    }
                }

                foreach (Triangle atv3 in Triangles[i].v3.adjacentTriangles)
                {
                    if (Triangles[i].v1.adjacentTriangles.Contains(atv3) && atv3 != Triangles[i])
                    {
                        Triangles[i].adjca = atv3;
                        Triangles[i].numNeighbours++;
                        break;
                    }
                }

                //			Debug.Log (triangles[i].adjab+" "+triangles [i].adjbc+" "+triangles[i].adjca);
                //			if (triangles [i].isEar)
                //				Debug.Log ("This triangle is an ear!");
                //			else if (triangles [i].isBorder)
                //				Debug.Log ("This is a border triangle");
            }
        }

        /// <summary>
        /// Converts the physical mesh to gameobjects (chunks) that are instantiated immediately.
        /// </summary>
        /// <returns>A reference to the container gameobject.</returns>
        /// <param name="mat">Material to display chunks.</param>
        /// <param name="strength">Strength of joints.</param>
        //public GameObject MeshToChunks(Material mat, float strength, PhysicsMaterial2D physics)
        //{
        //    GameObject whole = new GameObject();
        //    //create chunks
        //    GameObject[] objects = new GameObject[Triangles.Count];
        //    for (int i = 0; i < Triangles.Count; i++)
        //    {
        //        objects[i] = Triangles[i].TriangleToObject(mat, physics);
        //        if (objects[i] == null)
        //            continue;
        //        objects[i].name = "chunk" + i.ToString();
        //        objects[i].transform.parent = whole.transform;
        //        objects[i].GetComponent<TerrainChunk>().mesh = this;
        //        objects[i].GetComponent<TerrainChunk>().jointStrength = strength;
        //    }
        //    //create joints
        //    for (int i = 0; i < Triangles.Count; i++)
        //    {
        //        Triangle[] neighbours = new Triangle[3] { Triangles[i].adjab, Triangles[i].adjbc, Triangles[i].adjca };
        //        Vector2[] anchors = new Vector2[3] { Triangles[i].midab, Triangles[i].midbc, Triangles[i].midca };
        //        for (int k = 0; k < 3; k++)
        //        {
        //            if (neighbours[k] != null && neighbours[k].gameobject != null && Triangles.IndexOf(neighbours[k]) > i)
        //            {
        //                Triangles[i].gameobject.GetComponent<TerrainChunk>().MakeJoint(neighbours[k].gameobject, anchors[k]);
        //            }
        //        }
        //    }

        //    return whole;
        //}

        /// <summary>
        /// Converts physical mesh to unity Mesh object. (For nondestructable objects)
        /// </summary>
        /// <returns>The unity mesh.</returns>
        public UnityEngine.Mesh ToUnityMesh()
        {
            Vector3[] uvertices = new Vector3[Vertices.Count];
            for (int i = 0; i < Vertices.Count; i++)
            {
                uvertices[i] = Vertices[i].GetPosition();
            }

            int[] utriangles = new int[Triangles.Count * 3];
            for (int i = 0; i < Triangles.Count; i++)
            {
                utriangles[i * 3] = Vertices.IndexOf(Triangles[i].v1);
                utriangles[i * 3 + 1] = Vertices.IndexOf(Triangles[i].v2);
                utriangles[i * 3 + 2] = Vertices.IndexOf(Triangles[i].v3);
            }

            UnityEngine.Mesh umesh = new UnityEngine.Mesh();
            umesh.vertices = uvertices;
            umesh.triangles = utriangles;
            umesh.RecalculateNormals();
            umesh.RecalculateBounds();
            return umesh;
        }

        private void CheckFront(List<List<FrontVertex>> fronts)
        {
            FrontVertex v;
            for (int k = 0; k < fronts.Count; k++)
            {
                for (int i = 0; i < fronts[k].Count; i++)
                {
                    v = fronts[k][i];
                    if ((v.previousVertex == null) || (v.nextVertex == null))
                        throw new UnityException("Undefined neighbour at vertex " + i + ", front " + k);
                }
                v = fronts[k][0].nextVertex;
                int num = 1;
                while (v != fronts[k][0])
                {
                    if (!fronts[k].Contains(v))
                        throw new UnityException("Next vertex outside of the front " + k + ", vertex num " + num);
                    num++;
                    if (num > fronts[k].Count)
                        throw new UnityException("When going from one to another, more vertices than in the front " + k);
                    v = v.nextVertex;
                }
                if (num < fronts[k].Count)
                    throw new UnityException("When going from one to another, less vertices than in the front " + k);
            }
        }

        private void GenerateMesh(List<Vector2> polygon, List<List<FrontVertex>> fronts)
        {
            //make a list of FrontVertices from polygon
            fronts[0].AddRange(makeVertices(polygon));

            //sort vertices by angle
            fronts[0].Sort(FrontVertex.CompareByAngle);
            //		foreach (FrontVertex v in fronts[0])
            //			Debug.Log (v);

            //also add vertex to the list of mesh vertices
            foreach (FrontVertex fv in fronts[0])
                if (!Vertices.Contains(fv.thisVertex))
                    Vertices.Add(fv.thisVertex);
        }

        public void ContinueGenerating(List<List<FrontVertex>> fronts)
        {
            int skip = 0;

            while (fronts.Count > 0)
            {
                while (fronts[0].Count > 4)
                {
                    if (GenerateAMeshTriangle(fronts, fronts[0][skip]))
                    {
                        skip = 0;
                        fronts[0].Sort(FrontVertex.CompareByAngle);
                    }
                    else
                        skip++;
                    if (skip == fronts[0].Count)
                    {
                        Debug.Log("Bad Front");
                        foreach (FrontVertex v in fronts[0])
                            Debug.Log(v);
                        throw new UnityException("Couldn't triangulate this border!");
                    }
                }

                if (fronts[0].Count != 4)
                    throw new UnityException("Error generating mesh. Less than 4 last vertices");

                GenerateLastMeshTriangles(fronts[0]);
                skip = 0;
                fronts.RemoveAt(0);
            }

            CalculateNeighbours();
            serializableMesh = new SerializableMesh(this);
            Debug.Log("mesh created");

        }

        //		foreach (Vertex vertex in initVertices) {
        //			
        //
        //			if (front.Count == 0) {
        //				front.Add (vertex);
        //				continue;
        //			}
        //			int i;
        //			for (i = 0; i < front.Count; i++) {
        //				if (vertex.angle < front [i].angle)
        //					break;						
        //			}
        //			front.Insert (i, vertex);
        //		}

        /*
            int skip = 0;
            while (front.Count > 4) {
            if (GenerateAMeshTriangle (front, front[skip])) {
                    skip = 0;
                    front.Sort (Vertex.CompareByAngle);
                    }
                else
                    skip++;
                if (skip == front.Count)
                    throw new UnityException ("Couldn't triangulate this border!");
            }

            GenerateLastMeshTriangles (front);
    */

        public void GenerateLastMeshTriangles(List<FrontVertex> initfront)
        {
            Triangle newTriangle, newTriangle2;
            //sort the last 4 vertices in order (initially sorted by angle)
            FrontVertex[] front = new FrontVertex[4];
            front[0] = initfront[0];
            for (int k = 1; k < 4; k++)
                front[k] = front[k - 1].nextVertex;
            //make two final triangles
            if (front[0].angle + front[2].angle > front[1].angle + front[3].angle)
            {
                newTriangle = new Triangle(front[0], front[1], front[2]);
                newTriangle2 = new Triangle(front[0], front[2], front[3]);
            }
            else
            {
                newTriangle = new Triangle(front[0], front[1], front[3]);
                newTriangle2 = new Triangle(front[1], front[2], front[3]);
            }
            Triangles.Add(newTriangle);
            Triangles.Add(newTriangle2);
        }

        /// <summary>
        /// Generates one or two mesh triangles at the vertex v.
        /// </summary>
        /// <returns><c>true</c>, if triangles were generated, <c>false</c> otherwise.</returns>
        /// <param name="front">List of vertices.</param>
        /// <param name="v">Current vertex.</param>
        public bool GenerateAMeshTriangle(List<List<FrontVertex>> fronts, FrontVertex v)
        {
            Triangle newTriangle, newTriangle2;
            FrontVertex newVertex;
            List<FrontVertex> front = fronts[0];
            bool created = false;
            snappedIndex = -1;

            if (v.angle < 90)
            {
                //	casenum = 1;
                newTriangle = new Triangle(v.previousVertex, v, v.nextVertex);
                if (newTriangle.noVertexInside(front))
                {
                    Triangles.Add(newTriangle);
                    v.previousVertex.nextVertex = v.nextVertex;
                    v.nextVertex.previousVertex = v.previousVertex;
                    v.previousVertex.CalculateAngle();
                    v.nextVertex.CalculateAngle();
                    front.Remove(v);
                    created = true;
                }
            }
            else if (v.angle < 150)
            {
                //	casenum = 2;
                float vLength = ((v.previousVertex.GetPosition() - v.GetPosition()).magnitude + (v.nextVertex.GetPosition() - v.GetPosition()).magnitude) / 2f;
                vLength = Clamp(vLength, minChunkSize, maxChunkSize);
                Vector2 newVectorPosition = Quaternion.AngleAxis(v.angle / 2f, Vector3.forward) * (v.previousVertex.GetPosition() - v.GetPosition());
                newVectorPosition = newVectorPosition.normalized * vLength + v.GetPosition();
                snappedIndex = Snap(newVectorPosition, front);
                if (snappedIndex >= 0)
                {
                    newVertex = new FrontVertex(front[snappedIndex].thisVertex, v.previousVertex, v.nextVertex);
                }
                else
                {
                    newVertex = new FrontVertex(newVectorPosition, v.previousVertex, v.nextVertex);
                }
                newTriangle = new Triangle(v, newVertex, v.previousVertex);
                newTriangle2 = new Triangle(v, v.nextVertex, newVertex);
                if (newTriangle.noVertexInside(front) && newTriangle2.noVertexInside(front) && newTriangle.noEdgeIntersections(front) && newTriangle2.noEdgeIntersections(front))
                {
                    Triangles.Add(newTriangle);
                    Triangles.Add(newTriangle2);
                    newVertex.CalculateAndAdd(front);
                    v.previousVertex.nextVertex = newVertex;
                    v.nextVertex.previousVertex = newVertex;
                    v.previousVertex.CalculateAngle();
                    v.nextVertex.CalculateAngle();
                    front.Remove(v);
                    if (snappedIndex < 0)
                    {
                        Vertices.Add(newVertex.thisVertex);
                    }
                    else
                    {
                        SplitCurrentFront(fronts, newVertex);
                    }
                    created = true;
                }
            }
            else
            {
                //	casenum = 3;
                float vLength = ((v.previousVertex.GetPosition() - v.GetPosition()).magnitude + (v.nextVertex.GetPosition() - v.GetPosition()).magnitude) / 2f;
                vLength = Clamp(vLength, minChunkSize, maxChunkSize);
                Vector2 newVectorPosition = Quaternion.AngleAxis(v.angle - 60f, Vector3.forward) * (v.previousVertex.GetPosition() - v.GetPosition());
                newVectorPosition = newVectorPosition.normalized * vLength + v.GetPosition();
                snappedIndex = Snap(newVectorPosition, front);
                if (snappedIndex >= 0)
                {
                    newVertex = new FrontVertex(front[snappedIndex].thisVertex, v, v.nextVertex);
                }
                else
                {
                    newVertex = new FrontVertex(newVectorPosition, v, v.nextVertex);
                }
                newTriangle = new Triangle(v, v.nextVertex, newVertex);
                if (newTriangle.noVertexInside(front) && newTriangle.noEdgeIntersections(front))
                {
                    Triangles.Add(newTriangle);
                    newVertex.CalculateAndAdd(front);
                    v.nextVertex.previousVertex = newVertex;
                    v.nextVertex.CalculateAngle();
                    v.nextVertex = newVertex;
                    v.CalculateAngle();
                    if (snappedIndex < 0)
                    {
                        Vertices.Add(newVertex.thisVertex);
                    }
                    else
                    {
                        SplitCurrentFront(fronts, newVertex);
                    }
                    created = true;
                }
            }
            CheckFront(fronts);
            return created;
        }

        private int Snap(Vector2 newVertexPosition, List<FrontVertex> front)
        {
            for (int i = 0; i < front.Count; i++)
            {
                if ((newVertexPosition - front[i].GetPosition()).magnitude < minChunkSize)
                {
                    return i;
                }
            }
            return -1;
        }

        private void SplitCurrentFront(List<List<FrontVertex>> fronts, FrontVertex pivot)
        {
            FrontVertex current = pivot.nextVertex;
            List<FrontVertex> front = fronts[0];
            List<FrontVertex> newFront = new List<FrontVertex>();

            front.Remove(pivot);

            while (current.thisVertex != pivot.thisVertex)
            {
                front.Remove(current);

                if (front.Count == 0)
                {
                    throw new UnityException("Front is empty in SplitCurrentFront");
                }

                newFront.Add(current);
                current = current.nextVertex;
            }

            front.Remove(current);
            bool pivotIsBorder = pivot.thisVertex.isBorder;

            FrontVertex v1 = new FrontVertex(pivot.thisVertex, current.previousVertex, pivot.nextVertex, pivotIsBorder);
            newFront.Add(v1);
            v1.previousVertex.nextVertex = v1;
            v1.nextVertex.previousVertex = v1;

            FrontVertex v2 = new FrontVertex(pivot.thisVertex, pivot.previousVertex, current.nextVertex, pivotIsBorder);
            front.Add(v2);
            v2.previousVertex.nextVertex = v2;
            v2.nextVertex.previousVertex = v2;

            fronts.Add(newFront);

            foreach (List<FrontVertex> f in new List<FrontVertex>[] { front, newFront })
            {
                if (f.Count <= 4)
                {
                    if (f.Count == 3)
                    {
                        FrontVertex v = f[0];
                        Triangles.Add(new Triangle(v, v.previousVertex, v.nextVertex));
                    }
                    else if (f.Count == 4)
                    {
                        GenerateLastMeshTriangles(f);
                    }
                    fronts.Remove(f);
                }
            }
        }

        private List<FrontVertex> makeVertices(List<Vector2> polygon)
        {
            List<FrontVertex> vlist = new List<FrontVertex>(polygon.Count - 1);

            //Create Fronvertex objects from the list of points and mark them as border vertices
            for (int i = 0; i < polygon.Count - 1; i++)
            {
                vlist.Add(new FrontVertex(polygon[i]));
                vlist[i].thisVertex.MarkBorder();
            }

            for (int i = 0; i < polygon.Count - 1; i++)
            {
                //Mark neighbours for all Frontvertex object
                if (i == 0)
                    vlist[i].setNeighbours(vlist[polygon.Count - 2], vlist[i + 1]);
                else if (i == polygon.Count - 2)
                    vlist[i].setNeighbours(vlist[i - 1], vlist[0]);
                else
                    vlist[i].setNeighbours(vlist[i - 1], vlist[i + 1]);

                //Find repeated vertices and repair the link to Vertex object (thisvertex)
                for (int j = i + 1; j < polygon.Count - 1; j++)
                {
                    if (vlist[i].thisVertex.GetPosition() == vlist[j].thisVertex.GetPosition())
                    {
                        vlist[j].thisVertex = vlist[i].thisVertex;
                        Debug.Log("link repaired at " + vlist[i].thisVertex.GetPosition());
                        break;
                    }
                }
            }

            return vlist;
        }

        public float Clamp(float x, float min, float max)
        {
            if (min > max)
                throw new UnityException("Trying to clamp a number, but min>max");
            if (x < min)
                x = min;
            else if (x > max)
                x = max;
            return x;
        }
    }

    [Serializable]
    public class SerialVertex
    {
        public float x;
        public float y;
        public bool isBorder;

        public SerialVertex(Vertex vertex)
        {
            x = vertex.position.x;
            y = vertex.position.y;
            isBorder = vertex.isBorder;
        }
    }

    public class Vertex
    {
        public Vector2 position;
        public bool isBorder = false;
  //      public List<Vertex> adjacentVertices;
        public List<Triangle> adjacentTriangles;


        public Vertex(Vector2 _position)
        {
            position = _position;
     //       adjacentVertices = new List<Vertex>();
            adjacentTriangles = new List<Triangle>();
        }

        public Vertex(SerialVertex serialVertex)
        {
            this.isBorder = serialVertex.isBorder;
    //        adjacentVertices = new List<Vertex>();
            adjacentTriangles = new List<Triangle>();
            position = new Vector2(serialVertex.x, serialVertex.y);
        }

        public void MarkBorder()
        {
            isBorder = true;
        }

        /// <summary>
        /// Gets the vertex position.
        /// </summary>
        /// <returns>The Vector2 position.</returns>
        public Vector2 GetPosition()
        {
            return this.position;
        }

        public override string ToString()
        {
            return GetPosition().ToString();
        }

        /// <summary>
        /// Adds each vertex to another vertex's list of adjacent vertices, if not already added.
        /// </summary>
        /// <param name="v1">Vertex 1.</param>
        /// <param name="v2">Vertex 2.</param>
//        public static void MakeNeighbours(Vertex v1, Vertex v2)
//        {
//            if (!v1.adjacentVertices.Contains(v2))
//                v1.adjacentVertices.Add(v2);
//            if (!v2.adjacentVertices.Contains(v1))
//                v1.adjacentVertices.Add(v1);
//        }

		/// <summary>
		/// Sets up the geometric relations between the three vertices and a triangle made from them.
		/// </summary>
		/// <param name="v1">Vertex 1.</param>
		/// <param name="v2">Vertex 2.</param>
		/// <param name="v3">Vertex 3.</param>
		/// <param name="tri">Triangle.</param>
        public static void NewTriangle(Vertex v1, Vertex v2, Vertex v3, Triangle tri)
        {
            Vertex[] points = { v1, v2, v3 };
            foreach (Vertex v in points)
            {
                if (!v.adjacentTriangles.Contains(tri))
                    v.adjacentTriangles.Add(tri);
            }
        }

		/// <summary>
		/// Deletes the geometric relations between the triangle being deleted and its vertices.
		/// </summary>
		/// <param name="tri">Triangle.</param>
		public static void DeleteTriangle (Triangle tri) {
			//remove triangle from neighbouring vertices
			tri.v1.adjacentTriangles.Remove (tri);
			tri.v2.adjacentTriangles.Remove (tri);
			tri.v3.adjacentTriangles.Remove (tri);
		}


    }

    public class FrontVertex
    {
        public Vertex thisVertex;
        public FrontVertex previousVertex, nextVertex;
        public int angle;

        public FrontVertex(Vertex _thisVertex)
        {
            thisVertex = _thisVertex;
        }

        public FrontVertex(Vector2 _position)
        {
            thisVertex = new Vertex(_position);
        }

        public FrontVertex(Vertex _thisVertex, FrontVertex _previousVertex, FrontVertex _nextVertex) : this(_thisVertex)
        {
            setNeighbours(_previousVertex, _nextVertex);
        }

        public FrontVertex(Vector2 _position, FrontVertex _previousVertex, FrontVertex _nextVertex) : this(_position)
        {
            setNeighbours(_previousVertex, _nextVertex);
        }

        public FrontVertex(Vertex _thisVertex, FrontVertex _previousVertex, FrontVertex _nextVertex, bool _isBorder) : this(_thisVertex, _previousVertex, _nextVertex)
        {
            thisVertex.isBorder = _isBorder;
        }

        public void drawForward()
        {
            Gizmos.DrawLine(GetPosition(), nextVertex.GetPosition());
        }

        public void drawBackward()
        {
            Gizmos.DrawLine(GetPosition(), previousVertex.GetPosition());
        }

        public static int CompareByAngle(FrontVertex v1, FrontVertex v2)
        {
            return v1.angle.CompareTo(v2.angle);
        }

        public void setNeighbours(FrontVertex _previousVertex, FrontVertex _nextVertex)
        {
            previousVertex = _previousVertex;
            nextVertex = _nextVertex;
            CalculateAngle();
        }

        public Vector2 GetPosition()
        {
            return thisVertex.position;
        }

        public void CalculateAngle()
        {
            angle = (int)Vector2.Angle(previousVertex.GetPosition() - GetPosition(), nextVertex.GetPosition() - GetPosition());
            Vector3 cross = Vector3.Cross(previousVertex.GetPosition() - GetPosition(), nextVertex.GetPosition() - GetPosition());
            if (cross.z < 0)
                angle = 360 - angle;
        }

        public override String ToString()
        {
            return thisVertex.position.ToString() + ", " + angle.ToString();
        }

        public void CalculateAndAdd(List<FrontVertex> front)
        {
            CalculateAngle();
            if (!front.Contains(this))
                front.Add(this);
            //			int i;
            //			for (i = 0; i < front.Count; i++) {
            //				if (this.angle < front [i].angle)
            //					break;						
            //			}
            //			front.Insert (i, this);
        }

    }

    [Serializable]
    public class SerialTriangle
    {
        public int v1, v2, v3;

        public SerialTriangle(Triangle triangle, List<Vertex> listOfVertices)
        {
            v1 = listOfVertices.IndexOf(triangle.v1);
            v2 = listOfVertices.IndexOf(triangle.v2);
            v3 = listOfVertices.IndexOf(triangle.v3);
            if (v1 == -1 || v2 == -1 || v3 == -1)
                Debug.LogError("Error when trying to serialize the Mesh. One triangle contains a vertex that is not included in the list of mesh vertices. Please check the mesh generation code.");
        }
    }

    public class Triangle
    {
        //vertices
        public Vertex v1, v2, v3;
        private Vector2 a, b, c;
        //center of mass and center of sides
        public Vector2 center;
        public Vector2 midab, midbc, midca;
        //adjacent triangles
        public Triangle adjab, adjbc, adjca;
        //number or neighbours from the same body (1=ear, 2=border, 3=inner)
        public int numNeighbours = 0;
        //gameobject, created from this triangle
       // public GameObject gameobject;

        /// <summary>
        /// Initializes a new instance of the Triangle class. Determines relations between its vertices. Vertices must go in a clockwise direction!
        /// </summary>
        /// <param name="_v1">Vertex A.</param>
        /// <param name="_v2">Vertex B.</param>
        /// <param name="_v3">Vertex C.</param>
        public Triangle(Vertex _v1, Vertex _v2, Vertex _v3)
        {
            v1 = _v1;
            v2 = _v2;
            v3 = _v3;
            a = v1.GetPosition();
            b = v2.GetPosition();
            c = v3.GetPosition();
            if (Vector3.Cross(b - a, c - b).z == 0f)
                throw new UnityException("Trying to create triangle from three points on a line, coordinates: " + a + " " + b + " " + c);

            //find the center of mass - and the side centers in local system
            center = new Vector2((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3);
            midab = a - center + (b - a) / 2;
            midbc = b - center + (c - b) / 2;
            midca = c - center + (a - c) / 2;
           // Vertex.MakeNeighbours(v1, v2);
           // Vertex.MakeNeighbours(v2, v3);
           // Vertex.MakeNeighbours(v3, v1);
            Vertex.NewTriangle(v1, v2, v3, this);
        }

        public Triangle(FrontVertex _v1, FrontVertex _v2, FrontVertex _v3) : this(_v1.thisVertex, _v2.thisVertex, _v3.thisVertex) { }

        public Triangle[] GetNeighbours ()
        {
            Triangle[] neighbours = new Triangle[3];
            neighbours[0] = adjab;
            neighbours[1] = adjbc;
            neighbours[2] = adjca;
            return neighbours;
        }

		public Vector2[] GetPolygonPoints() {
			return new Vector2[] { (a), (b), (c) };
		}

		public void Delete() {
			//remove relations with vertices
			Vertex.DeleteTriangle (this);
			//remove relations with triangles
			if (adjab != null)
				adjab.RemoveNeighbour (this);
			if (adjbc != null)
				adjbc.RemoveNeighbour (this);
			if (adjca != null)
				adjca.RemoveNeighbour (this);
		}

		private void RemoveNeighbour (Triangle tri) {
			if (adjab == tri) {
				adjab = null;
				numNeighbours--;
			} else if (adjbc == tri) {
				adjbc = null;
				numNeighbours--;
			} else if (adjca == tri) {
				adjca = null;
				numNeighbours--;
			}
			else
				Debug.LogError ("Error deleting relations between triangles");
		}
        //public GameObject TriangleToObject(Material mat, PhysicsMaterial2D physics)
        //{
        //if (numNeighbours == 0)
        //    return null;

        ////find the center of mass - and the side centers in local system
        //center = new Vector2((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3);
        //midab = a - center + (b - a) / 2;
        //midbc = b - center + (c - b) / 2;
        //midca = c - center + (a - c) / 2;

        ////create a unity mesh for this triangle
        //UnityEngine.Mesh mesh = new UnityEngine.Mesh();
        //mesh.vertices = new Vector3[3] { a - center, b - center, c - center };
        //mesh.triangles = new int[3] { 0, 1, 2 };

        //GameObject obj = new GameObject();
        //obj.transform.position = center;
        //MeshFilter meshfilter = obj.AddComponent<MeshFilter>();
        //meshfilter.mesh = mesh;
        //MeshRenderer renderer = obj.AddComponent<MeshRenderer>();
        //renderer.material = mat;

        //Rigidbody2D rbody = obj.AddComponent<Rigidbody2D>();
        ////			rbody.drag = 1f;
        //rbody.mass = 2f;

        ////add script
        //TerrainChunk script = obj.AddComponent<TerrainChunk>();
        //script.triangle = this;
        //script.numNeighbours = this.numNeighbours;
        //script.GenerateCollider(a - center, b - center, c - center, physics);

        //collider here
        //			PolygonCollider2D collider = obj.AddComponent<PolygonCollider2D> ();
        //			collider.points = new Vector2[3] {(a-center), (b-center), (c-center)};

        /*		CircleCollider2D collider = obj.AddComponent<CircleCollider2D> ();
                //вписанная окружность
                float ab = (a - b).magnitude;
                float bc = (b - c).magnitude;
                float ca = (c - a).magnitude;
                float p = (ab + bc + ca) / 2f;
                collider.radius = Mathf.Sqrt((p-ab)*(p-bc)*(p-ca)/p);
                collider.offset = Vector2.zero;
                */
        //				
        //			collider.sharedMaterial = physics;

        //    gameobject = obj;
        //    return obj;
        //}

        /// <summary>
        /// Draws the triangle in the onDrawGizmos.
        /// </summary>
        public void drawTriangle()
        {
            Gizmos.DrawLine(a, b);
            Gizmos.DrawLine(b, c);
            Gizmos.DrawLine(c, a);
        }

        /// <summary>
        /// Checks to see if any edge derived from the given list of polygon vertices intersects this triangle.
        /// </summary>
        /// <returns><c>true</c>, if none of the edges intersects this triangle, <c>false</c> otherwise.</returns>
        /// <param name="vertices">List of vertices.</param>
        public bool noEdgeIntersections(List<FrontVertex> vertices)
        {
            foreach (FrontVertex vertex in vertices)
                if (this.IntersectsSegment(vertex.GetPosition(), vertex.nextVertex.GetPosition()))
                    return false;
            return true;
        }

        /// <summary>
        /// Checks to see if any polygon vertex from the given list lies inside or on the border of this triangle (except those coinciding with triangle vertices).
        /// </summary>
        /// <returns><c>true</c>, if none of the vertices is inside, <c>false</c> otherwise.</returns>
        /// <param name="vertices">List of vertices.</param>
        public bool noVertexInside(List<FrontVertex> vertices)
        {
            foreach (FrontVertex vertex in vertices)
            {
                if (vertex.GetPosition() == a || vertex.GetPosition() == b || vertex.GetPosition() == c)
                    continue;
                if (this.ContainsPoint(vertex.GetPosition()))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Checks whether a given line segment intersects the triangle. 
        /// </summary>
        /// <returns><c>true</c>, if segment intersects two edges of the triangle, <c>false</c> otherwise</returns>
        /// <param name="pointA">First point of line segment.</param>
        /// <param name="pointB">Second point of line segment.</param>
        public bool IntersectsSegment(Vector2 pointA, Vector2 pointB)
        {

            BarycentricPoint p1 = this.GetBarycentric(pointA);
            //	Debug.Log (p1.u.ToString () + " " + p1.v.ToString ());
            BarycentricPoint p2 = this.GetBarycentric(pointB);
            //	Debug.Log (p2.u.ToString () + " " + p2.v.ToString ());
            BarycentricPoint intersection;

            intersection.v = p1.v - p1.u * (p2.v - p1.v) / (p2.u - p1.u);
            intersection.u = p1.u - p1.v * (p2.u - p1.u) / (p2.v - p1.v);

            if (IsIntersectingEdge(p1.u, p2.u, intersection.v) || IsIntersectingEdge(p1.v, p2.v, intersection.u))
            {
                return true;
            }

            if (isZero(intersection.v) && isZero(intersection.u) &&
                 compareToZero(p1.v * p1.u) > 0 && compareToZero(p2.v * p2.u) > 0 && compareToZero(p1.v * p2.v) < 0)
                return true;
            if (IsPointAIntersection(p1, p2) || IsPointAIntersection(p2, p1))
                return true;
            return false;
        }

        /// <summary>
        /// Checks whether a given point is located inside the triangle.
        /// </summary>
        /// <returns><c>true</c>, if a point is located inside or on the triangle edges, <c>false</c> otherwise.</returns>
        /// <param name="point">Point.</param>
        public bool ContainsPoint(Vector2 point)
        {
            BarycentricPoint bpoint = this.GetBarycentric(point);
            // Check if point is in triangle
            return (compareToZero(bpoint.u) >= 0) && (compareToZero(bpoint.v) >= 0) && (compareTo(bpoint.u + bpoint.v, 1f) <= 0);
        }

        private bool IsIntersectingEdge(float c1, float c2, float intersection)
        {
            return ((compareToZero(c1 * c2) < 0) || (isZero(c1) && compareToZero(c2) > 0) || (isZero(c2) && compareToZero(c1) > 0)) &&
            (compareTo(intersection, 1f) < 0) && (compareToZero(intersection) > 0);
        }

        private bool IsPointAIntersection(BarycentricPoint p1, BarycentricPoint p2)
        {
            return (isZero(p1.u) && isZero(p1.v)) && (compareToZero(p2.u) > 0 && compareToZero(p2.v) > 0);
        }

        private bool isZero(float x)
        {
            return Mathf.Abs(x) <= Mesh.EPSILON;
        }

        private int compareToZero(float x)
        {
            if (Mathf.Abs(x) <= Mesh.EPSILON)
                return 0;
            else
                return Math.Sign(x);
        }

        private int compareTo(float x, float y)
        {
            if (Mathf.Abs(x - y) <= Mesh.EPSILON)
                return 0;
            else
                return Math.Sign(x - y);
        }

        /// <summary>
        /// A point with barycentric coordinates.
        /// </summary>
        private struct BarycentricPoint
        {
            public float u, v;

            /// <summary>
            /// Initializes a new instance of the <see cref="Tester+Triangle+BarycentricPoint"/> struct.
            /// </summary>
            /// <param name="_u">Barycentric coordinate U.</param>
            /// <param name="_v">Barycentric coordinate V.</param>
            public BarycentricPoint(float _u, float _v)
            {
                u = _u;
                v = _v;
            }
        }

        private BarycentricPoint GetBarycentric(Vector2 point)
        {
            Vector2 v2 = point - a;
            Vector2 v0 = c - a;
            Vector2 v1 = b - a;
            float dot00, dot01, dot02, dot11, dot12;

            // Compute dot products
            dot00 = Vector2.Dot(v0, v0);
            dot01 = Vector2.Dot(v0, v1);
            dot02 = Vector2.Dot(v0, v2);
            dot11 = Vector2.Dot(v1, v1);
            dot12 = Vector2.Dot(v1, v2);

            // Compute barycentric coordinates
            float invDenom = 1f / (dot00 * dot11 - dot01 * dot01);
            float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
            float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

            return new BarycentricPoint(u, v);
        }
    }
}


