﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Physical chunk derived from a mesh triangle.
/// </summary>
public class TerrainChunk : MonoBehaviour
{

    public float CrackingPoint;
    public float BreakingPoint;

    //TODO Hide all the other parameters in inspector
    public Body parent;
    public int ID { get; private set;}

    //depends on the number of neighbours: 0=island, 1=ear, 2=border, 3=inner
    //this value often determines the chunk physical behaviour
    public enum ChunkType { Island, Ear, Border, Inner };
    public ChunkType Type { get; private set; }

    //neighbouring Chunks
    public TerrainChunk[] neighbours = new TerrainChunk[3];

    //this is an important parameter that controls the destruction of chunks
    public bool stable = false;

    //this is a variable used for sorting in pathfinding
    public float distToStable = 0;

    //geometry
    public GeometryMesh.Mesh mesh;
    public GeometryMesh.Triangle triangle;

    //physical parameters
	public bool cracked = false;

	public float StaticStress;
	public float DynamicStress;
    
	public float Stress {
		get 
		{
			return StaticStress + DynamicStress;
		}
	}

    //public GameObject GameChunk { get; private set; }
    PolygonCollider2D polygonCollider;

	public bool CheckForBreaking () {
		if (!cracked && Stress >= CrackingPoint) {
			cracked = true;
			BreakingPoint = BreakingPoint / 2;
			//Debug.Log ("cracked chunk" + ID);
		} else if (Stress >= BreakingPoint) {
			//Debug.Log ("chunk"+ID+" shall be destroyed");
			return true;
		}
		return false;
	}

    public void ChangeNeighbour (TerrainChunk newNeighbour, TerrainChunk oldNeighbour)
    {
        for (int i = 0; i<3; i++)
        {
            if (neighbours[i] == oldNeighbour)
            {
                neighbours[i] = newNeighbour;
                Debug.Log("Succesfully changed neighbour of chunk"+ID);
            }
        }
    }

    public void SetType ()
    {
        ushort numNeighbours = 3;
        for (int i = 0; i<3; i++)
        {
            if (neighbours[i] == null)
                numNeighbours--;
        }
        Type = (ChunkType)numNeighbours;
        Debug.Log("for chunk"+ID+ " set type to "+Type);
    }

    public void Init(int ID, GeometryMesh.Triangle baseTriangle, Body body, GameObject parent = null)
    {
        //initialize
        BreakingPoint = CrackingPoint * 2 + 5f;
        this.parent = body;
        triangle = baseTriangle;
        StaticStress = 0f;
		DynamicStress = 0f;
        this.ID = ID;
		
        //detect type based on the geometry
        switch (triangle.numNeighbours)
        {
            case 0: 
                Type = ChunkType.Island;
                break;
            case 1: 
                Type = ChunkType.Ear;
                break;
            case 2: 
                Type = ChunkType.Border;
                break;
            case 3: 
                Type = ChunkType.Inner;
                break;
        }

        //render
        gameObject.name = "chunk" + ID.ToString();
		gameObject.tag = "Terrain";
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        // MeshRenderer render = GameChunk.AddComponent<MeshRenderer>();
        // gameObject.GetComponent<MeshRenderer>().material = material;
        meshFilter.mesh = new Mesh();
        Vector3[] meshvertices = new Vector3[3];
        meshvertices[0] = baseTriangle.v1.GetPosition();
        meshvertices[1] = baseTriangle.v2.GetPosition();
        meshvertices[2] = baseTriangle.v3.GetPosition();
        meshFilter.mesh.vertices = meshvertices;
        meshFilter.mesh.triangles = new int[] { 0, 1, 2 };
        meshFilter.mesh.RecalculateBounds();
        meshFilter.mesh.RecalculateNormals();

        if (parent) gameObject.transform.parent = parent.transform;

        polygonCollider = gameObject.GetComponent<PolygonCollider2D>();
        polygonCollider.points = triangle.GetPolygonPoints();
    }

    public void Init(int ID, Vector2[] corners, Body body, GameObject parent = null)
    {
        //initialize
        BreakingPoint = CrackingPoint * 2 + 5f;
        this.parent = body;
        StaticStress = 0f;
        DynamicStress = 0f;
        this.ID = ID;

       //render
        gameObject.name = "chunk" + ID.ToString();
        gameObject.tag = "Terrain";
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        // MeshRenderer render = GameChunk.AddComponent<MeshRenderer>();
        // gameObject.GetComponent<MeshRenderer>().material = material;
        meshFilter.mesh = new Mesh();
        Vector3[] meshvertices = new Vector3[3];
        meshvertices[0] = corners[0];
        meshvertices[1] = corners[1];
        meshvertices[2] = corners[2];
        meshFilter.mesh.vertices = meshvertices;
        meshFilter.mesh.triangles = new int[] { 0, 1, 2 };
        meshFilter.mesh.RecalculateBounds();
        meshFilter.mesh.RecalculateNormals();

        if (parent) gameObject.transform.parent = parent.transform;

        polygonCollider = gameObject.GetComponent<PolygonCollider2D>();
        polygonCollider.points = corners;
    }

    public Vector2[] GetCorners ()
    {
        Vector2[] corners = new Vector2[3];
        for (int i =0; i<3; i++)
        {
            corners[i] = polygonCollider.points[i] + (Vector2)transform.position;
        }
        return corners;
    }

    public void SetCorners (Vector2[] newCorners)
    {
        polygonCollider.points = newCorners;
        UnityEngine.Mesh umesh = GetComponent<MeshFilter>().mesh;
        umesh.vertices[0] = newCorners[0];
        umesh.vertices[1] = newCorners[1];
        umesh.vertices[2] = newCorners[2];
    }

    public void ColorChunk(Color color)
    {
        gameObject.GetComponent<MeshRenderer>().material.color = color;
    }

	/// <summary>
	/// Used when deleting the chunk: destroys relations with its neighbours and destroys the gameobject itself
	/// </summary>
	public void Delete () {
		for (int i = 0; i < 3; i++)
			if (neighbours[i] != null)
				neighbours [i].DeleteNeighbour (this);

        polygonCollider.enabled = false;
        Destroy(gameObject);
	}

	private void DeleteNeighbour (TerrainChunk chunk) {
		for (int i = 0; i < 3; i++)
			if (neighbours [i] == chunk) {
				neighbours [i] = null;
				Type--;
			}
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        //how hard is a collision? mass = 1 for each chunk
        float force = col.relativeVelocity.sqrMagnitude;
        //apply dynamic stress
        if (force > 0.5f)
        {
            Debug.Log("collision detected, " + force);
            parent.ApplyDynamicStress(this, force);

            //if collided with other chunk of terrain
            TerrainChunk other = col.gameObject.GetComponent<TerrainChunk>();
            if (other != null)
            {
                Debug.DrawLine(gameObject.transform.position, other.gameObject.transform.position, Color.green, 100f);
                other.parent.ApplyDynamicStress(other, force);
            }
        }
    }

    public ushort GetContactsWithOtherBodies (out ContactPoint2D[] contacts)
    {
        contacts = new ContactPoint2D[10];
        if (Type == ChunkType.Inner)
            return 0;
        ContactPoint2D[] points = new ContactPoint2D[10];
        int num = polygonCollider.GetContacts(points);
        if (num == 0) 
            return 0;
        TerrainChunk other;
        ushort numContacts = 0;
        for (int i = 0; i<num; i++)
        {
            //continue if in contact with something that is not a chunk
            other = points[i].collider.gameObject.GetComponent<TerrainChunk>();
            if (other == null)
                continue;
            //continue if the contacting chunks belong to the same body
            if (other.parent == this.parent)
                continue;
            //continue if the other chunk is an inner chunk
            if (other.Type == ChunkType.Inner)
                continue;

            Debug.Log("chunk" + ID + " in contact with " + points[i].collider.gameObject.name + ", " + points[i].point);
            numContacts++;
            contacts[i] = points[i];
        }
        return numContacts;
    }
}


