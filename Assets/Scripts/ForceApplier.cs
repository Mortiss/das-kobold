﻿using UnityEngine;
using System.Collections;

public class ForceApplier : MonoBehaviour {
	public float forceModifier = 5f;
	public float expModifier;
	public float radius = 0.3f;

	Vector2 lastMousePos;
	bool drag;

	// Use this for initialization
	void Start () {
	
	}

	void OnDrawGizmos () {
		Gizmos.color = Color.red;
		if (drag)
			Gizmos.DrawLine ((Vector3)lastMousePos, Camera.main.ScreenToWorldPoint (Input.mousePosition));
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 point = (Vector2)Camera.main.ScreenToWorldPoint (Input.mousePosition);


		if (Input.GetMouseButtonDown (0)) {
			lastMousePos = point;
			drag = true;
		}
		else if (Input.GetMouseButtonUp(0)) {
			Vector2 force = (point - lastMousePos)*forceModifier;
			RaycastHit2D hit = Physics2D.Raycast (lastMousePos, point-lastMousePos, (point-lastMousePos).magnitude);
			if (hit.rigidbody != null) {
				Debug.Log ("bang with force "+force);
				hit.rigidbody.AddForce (force);
			}
			drag = false;
		}

		if (Input.GetMouseButtonDown (1)) {
			Collider2D[] hits = Physics2D.OverlapCircleAll (point, radius);
			foreach (Collider2D hit in hits) {
				Vector2 force = ((Vector2)hit.transform.position-point)*expModifier;
				hit.attachedRigidbody.AddForce (force, ForceMode2D.Impulse);
			}
		}

	}
}
