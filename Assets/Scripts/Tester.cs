﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Tester : MonoBehaviour {

	public Vector2 triangleA, triangleB, triangleC;
	public Vector2 point, segment;
	Triangle tri;

	// Use this for initialization
	void OnValidate () {
		tri = new Triangle (triangleA, triangleB, triangleC);
//		Debug.Log ("Segment intersection "+tri.IntersectsSegment(point, segment));
//		print (Mathf.Epsilon);
	}

	void OnDrawGizmos () {
		Gizmos.color = Color.red;
		Gizmos.DrawLine (tri.a, tri.b);
		Gizmos.DrawLine (tri.b, tri.c);
		Gizmos.DrawLine (tri.c, tri.a);
		Gizmos.DrawLine (point, segment);
	}
	
	public class Triangle {
		//vertices
		public Vector2 a, b, c;
		//edges
		public Vector2 ab,bc,ca;
		//adjacent triangles
		public Triangle adjab, adjbc, adjca;
		//if one edge coincides with border
		public bool isBorder;
		//if two edges coincide with border
		public bool isEar;

		/// <summary>
		/// Initializes a new instance of the Triangle class. Vertices must go in a clockwise direction!
		/// </summary>
		/// <param name="_a">Vertex A.</param>
		/// <param name="_b">Vertex B.</param>
		/// <param name="_c">Vertex C.</param>
		public Triangle (Vector2 _a, Vector2 _b, Vector2 _c) {
			a = _a;
			b = _b;
			c = _c;
			ab = b-a;
			bc = c-b;
			ca = a-c;
			if (Vector3.Cross(ab,bc).z==0f)
				throw new UnityException ("Trying to create triangle from three points on a line, coordinates: "+a+" "+b+" "+c);
		}

		private bool IsIntersectingEdge (float c1, float c2, float intersection) {
			return ((compareToZero(c1 * c2) < 0) || (isZero(c1) && compareToZero(c2) > 0) || (isZero(c2) && compareToZero(c1) > 0)) &&
				(compareTo(intersection, 1f) < 0) && (compareToZero(intersection) > 0);
		}

		private bool IsPointAIntersection (BarycentricPoint p1, BarycentricPoint p2) {
			return (isZero (p1.u) && isZero (p1.v)) && (compareToZero (p2.u) > 0 && compareToZero (p2.v) > 0);
		}

		/// <summary>
		/// Checks whether a given line segment intersects the triangle. 
		/// </summary>
		/// <returns><c>true</c>, if segment intersects two edges of the triangle, <c>false</c> otherwise</returns>
		/// <param name="pointA">First point of line segment.</param>
		/// <param name="pointB">Second point of line segment.</param>
		public bool IntersectsSegment (Vector2 pointA, Vector2 pointB)	{

			BarycentricPoint p1 = this.GetBarycentric (pointA);
		//	Debug.Log (p1.u.ToString () + " " + p1.v.ToString ());
			BarycentricPoint p2 = this.GetBarycentric (pointB);
		//	Debug.Log (p2.u.ToString () + " " + p2.v.ToString ());
			BarycentricPoint intersection;

			intersection.v = p1.v - p1.u * (p2.v - p1.v) / (p2.u - p1.u);
			intersection.u = p1.u - p1.v * (p2.u - p1.u) / (p2.v - p1.v);

			if (IsIntersectingEdge (p1.u, p2.u, intersection.v) || IsIntersectingEdge (p1.v, p2.v, intersection.u)) {
				return true;
			}

			if (isZero(intersection.v) && isZero(intersection.u) && 
				compareToZero(p1.v * p1.u) > 0 && compareToZero(p2.v * p2.u) > 0 && compareToZero(p1.v * p2.v) < 0)
				return true;
			if (IsPointAIntersection(p1, p2) || IsPointAIntersection(p2, p1))
				return true;
			return false;
		}

		/// <summary>
		/// Checks whether a given point is located inside the triangle.
		/// </summary>
		/// <returns><c>true</c>, if a point is located inside or on the triangle edges, <c>false</c> otherwise.</returns>
		/// <param name="point">Point.</param>
		public bool ContainsPoint (Vector2 point) {
			BarycentricPoint bpoint = this.GetBarycentric (point);
			// Check if point is in triangle
			return (compareToZero(bpoint.u) >= 0) && (compareToZero(bpoint.v) >= 0) && (compareTo(bpoint.u + bpoint.v, 1f) <= 0);
		}

		private bool isZero (float x) {
			return Mathf.Abs (x) <= Mathf.Epsilon;
		}

		private int compareToZero (float x)	{
			if (Mathf.Abs (x) <= Mathf.Epsilon)
				return 0;
			else
				return Math.Sign (x);
		}

		private int compareTo (float x, float y) {
			if (Mathf.Abs (x-y) <= Mathf.Epsilon)
				return 0;
			else
				return Math.Sign (x-y);
		}

		/// <summary>
		/// A point with barycentric coordinates.
		/// </summary>
		private struct BarycentricPoint {
			public float u, v;
			/// <summary>
			/// Initializes a new instance of the <see cref="Tester+Triangle+BarycentricPoint"/> struct.
			/// </summary>
			/// <param name="_u">Barycentric coordinate U.</param>
			/// <param name="_v">Barycentric coordinate V.</param>
			public BarycentricPoint (float _u, float _v) {
				u = _u;
				v = _v;
			}
		}

		private BarycentricPoint GetBarycentric (Vector2 point)	{
			Vector2 v2 = point - a;
			Vector2 v0 = -ca;
			Vector2 v1 = ab;
			float dot00, dot01, dot02, dot11, dot12;

			// Compute dot products
			dot00 = Vector2.Dot (v0, v0);
			dot01 = Vector2.Dot (v0, v1);
			dot02 = Vector2.Dot (v0, v2);
			dot11 = Vector2.Dot (v1, v1);
			dot12 = Vector2.Dot (v1, v2);

			// Compute barycentric coordinates
			float invDenom = 1f / (dot00 * dot11 - dot01 * dot01);
			float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
			float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

			return new BarycentricPoint (u, v);
		}
	}
}
